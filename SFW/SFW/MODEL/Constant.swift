//
//  Constant.swift
//  JetPayApp
//
//  Created by My Mac on 2/14/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

struct ReversedGeoLocation {
    let name: String            // eg. Apple Inc.
    let streetName: String      // eg. Infinite Loop
   // let streetNumber: String    // eg. 1
    let city: String            // eg. Cupertino
    let state: String           // eg. CA
    let zipCode: String         // eg. 95014
    let country: String         // eg. United States
    let isoCountryCode: String  // eg. US

    var formattedAddress: String {
        return """
        \(name),
        \(streetName),
        \(city), \(state) \(zipCode)
        \(country)
        """
    }

    // Handle optionals as needed
    init(with placemark: CLPlacemark) {
        self.name           = placemark.name ?? ""
        self.streetName     = placemark.thoroughfare ?? ""
       // self.streetNumber   = placemark.subThoroughfare ?? ""
        self.city           = placemark.locality ?? ""
        self.state          = placemark.administrativeArea ?? ""
        self.zipCode        = placemark.postalCode ?? ""
        self.country        = placemark.country ?? ""
        self.isoCountryCode = placemark.isoCountryCode ?? ""
    }
}

struct MyClassConstants
{
    static var dictGlobalData: [String : AnyObject] = [:]
}

struct CustomerConstants
{
    static var user_id = String()
}

//MARK:- Screen Size

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
}

//MARK:- Custom fonts
struct FontNames {
    
//    static let RajdhaniName = "rajdhani"
//    struct Rajdhani {
        static let RajdhaniBold = "rajdhani-bold"
        static let RajdhaniMedium = "rajdhani-medium"
//    }
}
struct GlobalColor {
    static var SelectedColor = UserDefaults.standard.color(forKey: "ColorCode")
}
struct FontColor {
    
    static let dayfont = 0x000000
    static let nightfont = 0xffffff
    
   
}

struct GlobalVariables {
    static var globalarray = [[String:Any]]()
}

//MARK:- Webservices

 struct ServiceList
 {
 
    
    static let USERNAME = "admin"
    static let PASSWORD = "STUDENT@123"
    
//       static var SERVICE_URL =  "http://clientsdemoarea.com/projects/sfw/api/v1/"
   // static var SERVICE_URL = "http://localhost/sfw/api/v1/"
  //  static var SocketUrl = UserDefaults.standard.string(forKey: "ChatURL")
   // static var DomainUrl = UserDefaults.standard.string(forKey: "DomainName")
    
  //  static let TERMS_CONDITION_API = "terms-conditions?mobile=1"
    
    static var SERVICE_URL =  "https://www.junkminds.com/swf/api/v1/"
    
    static let SERVICE_AUTH = "auth/"
    static let X_SIMPLE_API_KEY = "ow400c888s4c8wck8w0c0w8co0kc00o0wgoosw80"
    static let AUTHORIZATION_AUTH = "YWRtaW46YWRtaW4="

    static let LOGIN_OTP_API = SERVICE_AUTH + "login_otp"
    
    static let APP_REGISTER_API = SERVICE_AUTH + "app_register"
    
    static let CHECK_LOGIN_OTP_API = SERVICE_AUTH + "check_login_otp"
    
    static let REGISTER_SEND_OTP_API = SERVICE_AUTH + "register_sendotp"
    
    static let LOGOUT_API = SERVICE_AUTH + "logout"
    
    
    static let STATE_CITY_API = "state/City"
    
  
    
    static let STATELIST_API = "state/State_list"
    
    static let STATE_STANDARD_LIST_API = "state/standard_list"
    
    static let STATE_ZIPCODE_API = "state/Zipcode"
       
    static let STATE_SKILL_LIST_API = "state/skill_list"
   
    static let JOB_DASHBOARD_API = "Dashbord"
    
    static let JOB_DETAIL_API = "Dashbord/job_detail"
    
    static let JOB_APPLY_API = "Dashbord/apply"
    
    static let JOB_LIKE_DISLIKE_API = "Dashbord/like"
    
    static let WISHLIST_API = "Dashbord/whishlist_list"
    
    static let USER_DETAIL_API = "Student"
    
    static let EDIT_PROFILE_API = "Student/edit_profile"
    
    
    
    static let INDUSTRY_LIST_API = "state/industry_list"
    
    static let JOBPOST_API = "Company/Jobpost"
      
    static let COMPANY_JOB_DETAIL_API = "Company/Dashbord/company_job_detail"
    
    static let JOB_LIST_API = "Company/Dashbord/company_job_list"
      
    static let COMPANY_PROFILE_API = "Profile"
    
    static let COMPANY_JOB_STATUS_API = "Company/Dashbord/job_status"
    
    static let GET_APPLY_JOB_API = "Dashbord/get_job_apply"
    
    static let COMPANY_FILTER_API = "Company/Company/company_filter"
    
    static let JOB_EDIT_API = "Company/Jobpost/job_edit"
    
   
    
    
    
//    static let OFFER_PRODUCT_API = "Product/offer_product"
//    
//    static let DELETE_PRODUCT_API = "product/delete_product"
//    
//    static let CHANGE_PRODUCT_PRICE_API = "product/change_product_price"
//    
//    static let ACTIVE_INACTIVE_API = "product/active_inactive"
//    
//     static let SALES_PERSON_API = "sales-person"
//    
//     static let ADD_SALES_PERSON_API = "sales-person/add_sales_person"
//    
//     static let SALES_PERSONS_ACTIVE_INACTIVE_API = "sales-person/active_inactive"
//  
//    static let LOCAL_PRODUCT_API = "product-local"
//    
//    static let ZIPCODE_API = "zipcode/zipcode"
 }

extension UserDefaults{
    
    func setIsLogin(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isUserLogin.rawValue)
    }
    
    func getIsLogin()-> Bool {
        return bool(forKey: UserDefaultsKeys.isUserLogin.rawValue)
    }
    
    func setUserDict(value: [String : Any]){
        set(value, forKey: UserDefaultsKeys.userData.rawValue)
    }
    
    func getUserDict() -> [String : Any]{
        return dictionary(forKey: UserDefaultsKeys.userData.rawValue) ?? [:]
    }
    
}

enum UserDefaultsKeys : String {
    case userData
    case isUserLogin
}

class StoredData: NSObject {
    
    static let shared = StoredData()
    var playerId: String!
   
}

// App Version Check


enum VersionError: Error {
    case invalidBundleInfo, invalidResponse
}

class LookupResult: Decodable {
    var results: [AppInfo]
}

class AppInfo: Decodable {
    var version: String
    var trackViewUrl: String
    //let identifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String,
    // You can add many thing based on "http://itunes.apple.com/lookup?bundleId=\(identifier)"  response
    // here version and trackViewUrl are key of URL response
    // so you can add all key beased on your requirement.

}

class ArgAppUpdater: NSObject {
    private static var _instance: ArgAppUpdater?;

    private override init() {

    }

    public static func getSingleton() -> ArgAppUpdater {
        if (ArgAppUpdater._instance == nil) {
            ArgAppUpdater._instance = ArgAppUpdater.init();
        }
        return ArgAppUpdater._instance!;
    }

    private func getAppInfo(completion: @escaping (AppInfo?, Error?) -> Void) -> URLSessionDataTask? {
        guard let identifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                DispatchQueue.main.async {
                    completion(nil, VersionError.invalidBundleInfo)
                }
                return nil
        }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                if let error = error { throw error }
                guard let data = data else { throw VersionError.invalidResponse }

                print("Data:::",data)
                print("response###",response!)

                let result = try JSONDecoder().decode(LookupResult.self, from: data)

                let dictionary = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)

                print("dictionary",dictionary!)


                guard let info = result.results.first else { throw VersionError.invalidResponse }
                print("result:::",result)
                completion(info, nil)
            } catch {
                completion(nil, error)
            }
        }
        task.resume()

        print("task ******", task)
        return task
    }
    private  func checkVersion(force: Bool) {
        let info = Bundle.main.infoDictionary
        let currentVersion = info?["CFBundleShortVersionString"] as? String
        _ = getAppInfo { (info, error) in

            let appStoreAppVersion = info?.version

            if let error = error {
                print(error)



            }else if appStoreAppVersion!.compare(currentVersion!, options: .numeric) == .orderedDescending {
                //                print("needs update")
               // print("hiiii")
                DispatchQueue.main.async {
                    let topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!

                    topController.showAppUpdateAlert(Version: (info?.version)!, Force: force, AppURL: (info?.trackViewUrl)!)
            }

            }
        }


    }

    func showUpdateWithConfirmation() {
        checkVersion(force : false)


    }

    func showUpdateWithForce() {
        checkVersion(force : true)
    }



}

extension UIViewController {


    fileprivate func showAppUpdateAlert( Version : String, Force: Bool, AppURL: String) {
        print("AppURL:::::",AppURL)

        let bundleName = Bundle.main.infoDictionary!["CFBundleName"] as! String;
        let alertMessage = "\(bundleName) Version \(Version) is available on AppStore."
        let alertTitle = "New Version"


        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)


        if !Force {
            let notNowButton = UIAlertAction(title: "Not Now", style: .default) { (action:UIAlertAction) in
                print("Don't Call API");


            }
            alertController.addAction(notNowButton)
        }

        let updateButton = UIAlertAction(title: "Update", style: .default) { (action:UIAlertAction) in
            print("Call API");
            print("No update")
            guard let url = URL(string: AppURL) else {
                return
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }

        }

        alertController.addAction(updateButton)
        self.present(alertController, animated: true, completion: nil)
    }
}

class SelfSizedTableView: UITableView {
  var maxHeight: CGFloat = UIScreen.main.bounds.size.height
  
  override func reloadData() {
    super.reloadData()
    self.invalidateIntrinsicContentSize()
    self.layoutIfNeeded()
  }
  
  override var intrinsicContentSize: CGSize {
    let height = min(contentSize.height, maxHeight)
    return CGSize(width: contentSize.width, height: height)
  }
}



private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
               return 10 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = String(t?.prefix(maxLength) ?? "")
    }
}
