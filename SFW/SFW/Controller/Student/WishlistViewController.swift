//
//  WishlistViewController.swift
//  SFW
//
//  Created by My Mac on 30/09/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable
import SideMenu

class wishlistcell: UITableViewCell {
    @IBOutlet var lbljobcode: UILabel!
    @IBOutlet var lblpostname: UILabel!
    @IBOutlet var lblcmpname: UILabel!
    @IBOutlet var lblexp: UILabel!
    @IBOutlet var lbllocation: UILabel!
    @IBOutlet var lblrequirement: UILabel!
  
       
    @IBOutlet var lblqualification: UILabel!
       @IBOutlet var lblduration: UILabel!
       @IBOutlet var lbljobtiming: UILabel!
       @IBOutlet var lblstartdt: UILabel!
        @IBOutlet var lblenddt: UILabel!
        @IBOutlet var lblopenfor: UILabel!
        @IBOutlet var lblpayscale: UILabel!
        @IBOutlet var lblpaymentmode: UILabel!
        @IBOutlet var lblincentives: UILabel!
        @IBOutlet var lblmono: UILabel!
        @IBOutlet var lblemail: UILabel!
        @IBOutlet var lbladdress: UILabel!
        @IBOutlet var lblcontactby: UILabel!
    
    @IBOutlet var textvwdescr: UITextView!
    
    @IBOutlet var btnlike: AnimatableButton!
    @IBOutlet var btnapply: AnimatableButton!
    
      var btnLikeAction : (()->())?
      var btnApplyAction : (()->())?
    
    @IBAction func like_click(_ sender: AnimatableButton) {
        btnLikeAction?()
    }
    @IBAction func apply_click(_ sender: AnimatableButton) {
        btnApplyAction?()
    }
    
}


class WishlistViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tblwishlist: UITableView!
    @IBOutlet weak var vwnodata: UIView!
    @IBOutlet weak var nodataimg: UIImageView!
    @IBOutlet weak var lblnodata: UILabel!
    var wishlist = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        WishlistApi()
    }
    
    //MARK: - Tableview Data Source
                    
                    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                     
                       return wishlist.count
                       
                    }
                    
                    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                     
                        let cell = tableView.dequeueReusableCell(withIdentifier: "wishlistcell", for: indexPath) as! wishlistcell
                       let dict:NSDictionary = wishlist[indexPath.row] as NSDictionary
                       
                        cell.lbljobcode.text = "# \(dict["JobCode"] as? String ?? "")"
                        cell.lblpostname.text = dict["JobTittle"] as? String
                        cell.lblcmpname.text = dict["CompanyName"] as? String
                        cell.lblexp.text = dict["Work_experience"] as? String
                        cell.lbllocation.text = dict["City_Name"] as? String
                        cell.lblrequirement.text = dict["SkillIds"] as? String
//                        cell.lblqualification.text = dict["Qualification"] as? String
//                        cell.lbljobtiming.text = dict["JobTiming"] as? String
//                        cell.lblmono.text = dict["PhoneContact"] as? String
//                        cell.lblemail.text = dict["EmailIDs"] as? String
//                        cell.lbladdress.text = dict["Address1"] as? String
                        
                        cell.lblqualification.text = dict["Qualification"] as? String
                        cell.lblduration.text = dict["Duration"] as? String
                        cell.lbljobtiming.text = dict["JobTiming"] as? String
                        cell.lblstartdt.text = dict["StartDate"] as? String
                        cell.lblenddt.text = dict["EndDate"] as? String
                        cell.lblopenfor.text = dict["OpenFor"] as? String
                        cell.lblpayscale.text = "\(dict["PayScale"] as? String ?? "") \((dict["PaymentSchedule"] as? String ?? ""))"
                        cell.lblpaymentmode.text = dict["PayMode"] as? String
                        cell.lblincentives.text = dict["AdditionalIncentives"] as? String
                        cell.lblmono.text = dict["PhoneContact"] as? String
                        cell.lblemail.text = dict["EmailIDs"] as? String
                        cell.lbladdress.text = dict["Address1"] as? String
                        cell.lblcontactby.text = dict["Candidates_Contact"] as? String
                       
                        cell.textvwdescr.text = dict["Description"] as? String
                        cell.lblqualification.text = dict["Qualification"] as? String
                        
                        let tapemail = UITapGestureRecognizer(target: self, action: #selector(WishlistViewController.tapEmail))
                        cell.lblemail.isUserInteractionEnabled = true
                        cell.lblemail.addGestureRecognizer(tapemail)
                        
                       let tap = UITapGestureRecognizer(target: self, action: #selector(WishlistViewController.tapFunction))
                        cell.lblmono.isUserInteractionEnabled = true
                        cell.lblmono.addGestureRecognizer(tap)
                       
                       let likes = dict["likes"] as? String
                       let apply = dict["apply"] as? String
                       
                       if likes == "1" {
                           cell.btnlike.setTitle("DISLIKE", for: .normal)
                       }
                       else
                       {
                           cell.btnlike.setTitle("LIKE", for: .normal)
                       }
                       
                       if apply == "1" {
                           cell.btnapply.setTitle("CANCEL", for: .normal)
                       }
                       else
                       {
                           cell.btnapply.setTitle("APPLY", for: .normal)
                       }
                        
                          cell.btnLikeAction = { () in
                            self.JobLikeDislikeApi(islike: "0", JobID: dict["JobID"] as? String ?? "0")
                        }
                        
                          cell.btnApplyAction = { () in
                           
                            if apply == "1" {
                                 self.JobApplyApi(isapply: "0", JobID: dict["JobID"] as? String ?? "0")
                            }
                            else
                            {
                                 self.JobApplyApi(isapply: "1", JobID: dict["JobID"] as? String ?? "0")
                            }
                        }
                       
               
                        return cell
           }
    
    @objc
       func tapEmail(sender:UITapGestureRecognizer) {
           
        let lbl = sender.view as! UILabel
        print("tap working",lbl.text ?? "")
        let email = lbl.text ?? ""
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
       }
    
    @objc
       func tapFunction(sender:UITapGestureRecognizer) {
           
        let lbl = sender.view as! UILabel
        print("tap working",lbl.text ?? "")
       // let dict:NSDictionary = companylist[sender.tag] as NSDictionary
        callNumber(phoneNumber: lbl.text ?? "")
       }
    
    private func callNumber(phoneNumber:String) {

        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {

            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                     application.openURL(phoneCallURL as URL)

                }
            }
        }
    }
       

  //MARK: - Button Action
    
    @IBAction func menu_click(_ sender: UIButton) {
             let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! SideMenuNavigationController
                    menu.presentationStyle.menuStartAlpha = 1.0
                    menu.presentationStyle.presentingEndAlpha = 0.5
                    present(menu, animated: true, completion: nil)
         }
    
    
    // MARK: - API
                
                  func WishlistApi()
                                         {
                                             if !isInternetAvailable(){
                                                 noInternetConnectionAlert(uiview: self)
                                             }
                                             else
                                             {
                                                let url = ServiceList.SERVICE_URL+ServiceList.WISHLIST_API
                                               
                                               let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                          "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                          ] as [String : Any]
                                               
                                                 let parameters = [:
                                                 ] as [String : Any]
                               
                                                  callApi(url,
                                                                method: .post,
                                                                param: parameters ,
                                                                extraHeader: header,
                                                               withLoader: true)
                                                        { (result) in
                                                              print("\(ServiceList.WISHLIST_API)==>RESPONSE:",result)
                                                            
                                                             if result.getBool(key: "status")
                                                             {
                                                                 let res = result.strippingNulls()
                                                                 let data = res["data"] as? [String : Any]
                                                                 
                                                                self.wishlist = data?["whishlist"] as? [[String : Any]] ?? []
                                                                //self.tblwishlist.reloadData()
                                                                if self.wishlist.count == 0 {
                                                                    showToast(uiview: self, msg: result.getString(key: "message"))
                                                                }
                                                                if self.wishlist.count > 0 {
                                                                    self.tblwishlist.reloadData()
                                                                    self.vwnodata.isHidden = true
                                                                    self.tblwishlist.isHidden = false
                                                                }
                                                                else
                                                                {
                                                                    self.tblwishlist.isHidden = true
                                                                    self.vwnodata.isHidden = false
                                                                    self.nodataimg.sd_setImage(with: URL(string: UserDefaults.standard.getUserDict()["ProfileImage"] as? String ?? ""), placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
                                                                    self.lblnodata.text = result.getString(key: "message")
                                                                }
                                                             }
                                                          else
                                                          {
                                                              showToast(uiview: self, msg: result.getString(key: "message"))
                                                          }
                                                 }
                                             }
                                             
                                         }
       
    func JobLikeDislikeApi(islike:String,JobID:String)
                                           {
                                               if !isInternetAvailable(){
                                                   noInternetConnectionAlert(uiview: self)
                                               }
                                               else
                                               {
                                                  let url = ServiceList.SERVICE_URL+ServiceList.JOB_LIKE_DISLIKE_API
                                                 
                                                 let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                            "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                            ] as [String : Any]
                                                 
                                                   let parameters = [ "JobID":JobID,
                                                                      "is_like":islike
                                                                      
                                                   ] as [String : Any]
                                 
                                                    callApi(url,
                                                                  method: .post,
                                                                  param: parameters ,
                                                                  extraHeader: header,
                                                                 withLoader: true)
                                                          { (result) in
                                                                print("\(ServiceList.JOB_LIKE_DISLIKE_API)==>RESPONSE:",result)
                                                              
                                                               if result.getBool(key: "status")
                                                               {
                                                                   let res = result.strippingNulls()
                                                              //     let data = res["data"] as? [String : Any]
                                                                   
                                                             //    self.jobdata["likes"] = res["is_like"]
                                                                // self.setdata()
                                                                   
//                                                                   if res["is_like"] as! Int == 1 {
//                                                                       self.btnlike.setTitle("DISLIKE", for: .normal)
//                                                                   }
//                                                                   else
//                                                                   {
//                                                                       self.btnlike.setTitle("LIKE", for: .normal)
//                                                                   }
                                                                self.WishlistApi()
                                                                   showToast(uiview: self, msg: result.getString(key: "message"))
                                                               }
                                                            else
                                                            {
                                                                showToast(uiview: self, msg: result.getString(key: "message"))
                                                            }
                                                   }
                                               }
                                               
                                           }
       
    func JobApplyApi(isapply:String,JobID:String)
                                              {
                                                  if !isInternetAvailable(){
                                                      noInternetConnectionAlert(uiview: self)
                                                  }
                                                  else
                                                  {
                                                     let url = ServiceList.SERVICE_URL+ServiceList.JOB_APPLY_API
                                                    
                                                    let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                               "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                               ] as [String : Any]
                                                    
                                                      let parameters = [ "JobID":JobID,
                                                                         "is_apply":isapply
                                                                         
                                                      ] as [String : Any]
                                    
                                                       callApi(url,
                                                                     method: .post,
                                                                     param: parameters ,
                                                                     extraHeader: header,
                                                                    withLoader: true)
                                                             { (result) in
                                                                   print("\(ServiceList.JOB_APPLY_API)==>RESPONSE:",result)
                                                                 
                                                                  if result.getBool(key: "status")
                                                                  {
                                                                      let res = result.strippingNulls()
                                                         
                                                                    self.WishlistApi()
                                                                   showToast(uiview: self, msg: result.getString(key: "message"))
                                                                  }
                                                               else
                                                               {
                                                                   showToast(uiview: self, msg: result.getString(key: "message"))
                                                               }
                                                      }
                                                  }
                                                  
                                              }
       

}
