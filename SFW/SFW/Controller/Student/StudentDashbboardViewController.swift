//
//  StudentDashbboardViewController.swift
//  SFW
//
//  Created by My Mac on 24/09/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SideMenu
import IBAnimatable
import iOSDropDown

class Studentdashboardcell: UITableViewCell {
    @IBOutlet var lblpostname: UILabel!
    @IBOutlet var lblcmpname: UILabel!
    @IBOutlet var lblexp: UILabel!
    @IBOutlet var lbllocation: UILabel!
    @IBOutlet var lblrequirement: UILabel!
    @IBOutlet var lbltime: UILabel!
    @IBOutlet var lblqualification: UILabel!
    @IBOutlet var lblemail: UILabel!
    @IBOutlet var btnpdf: UIButton!
    @IBOutlet var lblnumber: UILabel!
    @IBOutlet var lblcitycount: UILabel!
}

class StudentDashbboardViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tbllist: UITableView!
    
    @IBOutlet var filtervw: UIView!
    @IBOutlet var txtcity: DropDown!
    @IBOutlet var txtskill: DropDown!
    @IBOutlet var txtjobtype: DropDown!
    @IBOutlet var txtzipcode: UITextField!
    @IBOutlet var txtkeyword: UITextField!
    @IBOutlet var lblcitycount: UILabel!
    @IBOutlet var lblskillcount: UILabel!
    @IBOutlet weak var vwnodata: UIView!
    @IBOutlet weak var nodataimg: UIImageView!
    @IBOutlet weak var lblnodata: UILabel!
    
    var companylist = [[String:Any]]()
    var SkillID = String()
    var CityID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        txtjobtype.optionArray = ["Hourly","Daily","Weekly","Monthly"]
        filtervw.isHidden = true
        StudentDashboardApi()
        SkillApi()
        CityApi()
        DropdownClick()
    }
    
     //MARK: - Tableview Data Source
                 
                 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                  
                    return companylist.count
                    
                 }
                 
                 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                  
                     let cell = tableView.dequeueReusableCell(withIdentifier: "Studentdashboardcell", for: indexPath) as! Studentdashboardcell
                    let dict:NSDictionary = companylist[indexPath.row] as NSDictionary
                    
                    cell.lblnumber.text = "# \(dict["JobCode"] as? String ?? "")"
                    cell.lblpostname.text = dict["JobTittle"] as? String
                    cell.lblcmpname.text = dict["CompanyName"] as? String
                    cell.lblexp.text = dict["Work_experience"] as? String
                    cell.lbllocation.text = dict["City_Name"] as? String
                    cell.lblrequirement.text = dict["SkillIds"] as? String
                    cell.lblqualification.text = dict["Qualification"] as? String
                    
            
                     return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let dict:NSDictionary = companylist[indexPath.row] as NSDictionary
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "JobDetailViewController") as! JobDetailViewController
        nextViewController.JobID = dict["JobID"] as! String
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
     //MARK: - Button Action
    
    @IBAction func menu_click(_ sender: UIButton) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! SideMenuNavigationController
        menu.presentationStyle.menuStartAlpha = 1.0
        menu.presentationStyle.presentingEndAlpha = 0.5
        present(menu, animated: true, completion: nil)
    }
    
    @IBAction func citywise_click(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MatchingListViewController") as! MatchingListViewController
        nextViewController.CityID =  UserDefaults.standard.getUserDict()["CityID"] as? String ?? ""
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func skillwise_click(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MatchingListViewController") as! MatchingListViewController
        nextViewController.SkillID =  UserDefaults.standard.getUserDict()["Skill"] as? String ?? ""
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func filter_click(_ sender: AnimatableButton) {
        self.showViewWithAnimation(vw: filtervw, img: UIImageView())
    }
    @IBAction func apply_click(_ sender: AnimatableButton) {
        self.hideViewWithAnimation(vw: filtervw, img: UIImageView())
        StudentDashboardApi()
    }
    
    @IBAction func cleare_click(_ sender: AnimatableButton) {
        self.hideViewWithAnimation(vw: filtervw, img: UIImageView())
        ClearFilter()
        StudentDashboardApi()
    }
    @IBAction func close_click(_ sender: AnimatableButton) {
        self.hideViewWithAnimation(vw: filtervw, img: UIImageView())
    }
    
    func ClearFilter()  {
        txtcity.text = ""
        txtskill.text = ""
        txtjobtype.text = ""
        txtzipcode.text = ""
        txtkeyword.text = ""
        SkillID = ""
        CityID = ""
        
    }
    
    func DropdownClick()  {
           
           txtskill.didSelect(completion: { (selected, index, id)  in
                                   print(selected, index, id)
                                   self.SkillID = String(selected)
                               })
           
        
           
           txtcity.didSelect(completion: { (selected, index, id)  in
                      print(selected, index, id)
                self.CityID = String(id)
                  })
    }
    
    
    // MARK: - API
    
    func CityApi()
                           {
                               if !isInternetAvailable(){
                                   noInternetConnectionAlert(uiview: self)
                               }
                               else
                               {
                                  
                              let parameters = ["StateID":""] as [String : Any]
                                
                                let url = ServiceList.SERVICE_URL+ServiceList.STATE_CITY_API
                                
                                    callApi(url,
                                            method: .post,
                                            param: parameters ,
                                            withLoader: true)
                                          { (result) in
                                               print("\(ServiceList.STATE_CITY_API)==>RESPONSE:",result)
                                               if result.getBool(key: "status")
                                               {
                                                   let dict = result["data"] as? [String:Any] ?? [:]
                                                   let arr = dict.getArrayofDictionary(key: "city")
                                                   self.txtcity.optionArray = arr.compactMap {"\($0["Name"] as? String ?? "0")"}
                                                   self.txtcity.optionIds =  arr.compactMap {Int( $0["ID"] as? String ?? "0")}
                                               }
                                            else
                                               {
                                                showToast(uiview: self, msg: result.getString(key: "message"))
                                            }
                                   }
                               }
                               
                           }
    
     func SkillApi()
                                    {
                                        if !isInternetAvailable(){
                                            noInternetConnectionAlert(uiview: self)
                                        }
                                        else
                                        {
                                           
                                       let parameters = ["CityID":""] as [String : Any]
                                         
                                         let url = ServiceList.SERVICE_URL+ServiceList.STATE_SKILL_LIST_API
                                         
                                             callApi(url,
                                                     method: .get,
                                                     param: parameters ,
                                                     withLoader: true)
                                                   { (result) in
                                                        print("\(ServiceList.STATE_SKILL_LIST_API)==>RESPONSE:",result)
                                                        if result.getBool(key: "status")
                                                        {
                                                            let dict = result["data"] as? [String:Any] ?? [:]
                                                            let arr = dict.getArrayofDictionary(key: "Skill")

                                                            self.txtskill.optionArray = arr.compactMap {"\($0["Skill"] as? String ?? "0")"}
                                                            self.txtskill.optionIds =  arr.compactMap {Int( $0["SkillID"] as? String ?? "0")}
                                                        }
                                                     else
                                                        {
                                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                                     }
                                            }
                                        }
                                        
                                    }
           
             func StudentDashboardApi()
                                    {
                                        if !isInternetAvailable(){
                                            noInternetConnectionAlert(uiview: self)
                                        }
                                        else
                                        {
                                           let url = ServiceList.SERVICE_URL+ServiceList.JOB_DASHBOARD_API
                                          
                                          let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                     "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                     ] as [String : Any]
                                          
                                            let parameters = [ "CityID":CityID,
                                                               "Skill":SkillID,
                                                               "Duration":txtjobtype.text ?? "",
                                                               "ZipCode":txtzipcode.text ?? "",
                                                               "keywords":txtkeyword.text ?? ""
                                            ] as [String : Any]
                          
                                             callApi(url,
                                                           method: .post,
                                                           param: parameters ,
                                                           extraHeader: header,
                                                          withLoader: true)
                                             { [self] (result) in
                                                         print("\(ServiceList.JOB_DASHBOARD_API)==>RESPONSE:",result)
                                                       
                                                        if result.getBool(key: "status")
                                                        {
                                                            let res = result.strippingNulls()
                                                            let data = res["data"] as? [String : Any]
                                                            
                                                            self.companylist = data?["Job"] as? [[String : Any]] ?? []
                                                            self.lblcitycount.text = "\(result.getInt(key: "CityCount"))"
                                                            self.lblskillcount.text = "\(result.getInt(key: "skills"))"
                                                            
                                                            if self.companylist.count > 0 {
                                                                self.tbllist.reloadData()
                                                                self.vwnodata.isHidden = true
                                                                self.tbllist.isHidden = false
                                                            }
                                                            else
                                                            {
                                                                self.tbllist.isHidden = true
                                                                self.vwnodata.isHidden = false
                                                                self.nodataimg.sd_setImage(with: URL(string: UserDefaults.standard.getUserDict()["ProfileImage"] as? String ?? ""), placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
                                                                self.lblnodata.text = result.getString(key: "message")
                                                            }
                                                           
                                                        }
                                                     else
                                                     {
                                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                                     }
                                            }
                                        }
                                        
                                    }
    

}
