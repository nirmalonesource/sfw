//
//  StudentRegistrationViewController.swift
//  SFW
//
//  Created by My Mac on 21/09/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable
import SkyFloatingLabelTextField
import M13Checkbox
import iOSDropDown
import TagListView
import SwiftyPickerPopover
import MobileCoreServices

class StudentRegistrationViewController: UIViewController,TagListViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet var txtfname: SkyFloatingLabelTextField!
    @IBOutlet var txtlname: SkyFloatingLabelTextField!
    @IBOutlet var txtcontactno: SkyFloatingLabelTextField!
    @IBOutlet var txtemail: SkyFloatingLabelTextField!
    @IBOutlet var chkmale: M13Checkbox!
    @IBOutlet var chkfemale: M13Checkbox!
    @IBOutlet var txtdob: SkyFloatingLabelTextField!
    @IBOutlet var txtaddress: SkyFloatingLabelTextField!
    @IBOutlet var txtcity: DropDown!
    @IBOutlet var txtstate: DropDown!
    @IBOutlet var txtzipcode: DropDown!
    @IBOutlet var zipcodetagvw: TagListView!
    @IBOutlet var txtschool: SkyFloatingLabelTextField!
    @IBOutlet var txtclass: DropDown!
    @IBOutlet var txtreferencestudentname: SkyFloatingLabelTextField!
    @IBOutlet var txtreferencestudentcontactno: SkyFloatingLabelTextField!
    @IBOutlet var txtkeywordd: SkyFloatingLabelTextField!
    @IBOutlet var txtskill: DropDown!
    @IBOutlet var skilltagvw: TagListView!
    @IBOutlet var txtworkexp:  DropDown!
    @IBOutlet var txtresume: SkyFloatingLabelTextField!
    
    @IBOutlet var mystate: DropDown!
    @IBOutlet var mycity: DropDown!
    @IBOutlet var myzipcode: DropDown!
    @IBOutlet var btnprofile: AnimatableButton!
    @IBOutlet var imglogo: UIImageView!
    @IBOutlet var imgprofile: UIImageView!
    @IBOutlet var btnsubmit: AnimatableButton!
    @IBOutlet var lblaccount: UILabel!
    @IBOutlet var btnlogin: UIButton!
    @IBOutlet var lblregister: UILabel!
    
     var strradio = String()
     var StateList = [[String:Any]]()
     var CityList = [[String:Any]]()
    var ZipcodeList = [[String:Any]]()
    var SkillList = [[String:Any]]()
    var selectedZipcodeList = [[String:Any]]()
    var selectedSkillList = [[String:Any]]()
    var selectedEditZipcodeList = [String]()
    var selectedEditSkillList = [String]()
    var StateID = String()
    var CityID = String()
    var ResumePath = String()
     var QualificationID = String()
    var StandardID = String()
    var myStateID = String()
    var myCityID = String()
    
    var editdict = [String:Any]()
    var isedit = false
      var picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DropdownClick()
        SetupRadioButton()
        SetupTaglistview()
        
        StateApi()
        SkillApi()
        StandardListApi()
        
        if isedit {
            lblregister.text = "EDIT PROFILE"
            btnsubmit.setTitle("UPDATE", for: .normal)
            btnprofile.isHidden = false
            imglogo.isHidden = true
            btnlogin.isHidden = true
            lblaccount.isHidden = true
            txtcontactno.isEnabled = false
            SetData()
        }
        else
        {
            chkmale?.checkState = .checked
            strradio = "Male"
        }
        
         picker.delegate = self
        
        txtworkexp.optionArray = ["0-2 yrs","2-4 yrs","4-6 yrs","6-8 yrs","8-10 yrs","10+ yrs"]
    }
    
    func SetData()  {
        btnprofile.isHidden = false
        imglogo.isHidden = true
        imgprofile.sd_setImage(with: URL(string: editdict["ProfileImage"] as? String ?? ""), placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
      //  btnprofile?.sd_setImage(with: URL(string: editdict["ProfileImage"] as? String ?? ""), for: .normal, placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
        txtfname.text = editdict["FName"] as? String
        txtlname.text = editdict["LName"] as? String
        txtcontactno.text = editdict["mobile_no"] as? String
        txtemail.text = editdict["email"] as? String
        if editdict["Gender"] as? String == "Male" {
            chkmale?.checkState = .checked
            strradio = "Male"
        }
        else
        {
            chkfemale.checkState = .checked
            strradio = "Female"
        }
        txtdob.text = editdict["Dob"] as? String
        txtaddress.text = editdict["Address1"] as? String
        txtstate.text = editdict["mystate"] as? String
        txtcity.text = editdict["mycity"] as? String
        myzipcode.text = editdict["myzipcode"] as? String
        txtschool.text = editdict["LastInstituteName"] as? String
        txtclass.text = editdict["Standard"] as? String
        
        mystate.text = editdict["State_Name"] as? String
        mycity.text = editdict["City_Name"] as? String
        
        let Zipcode = editdict["Zipcode"] as? String
        let ziparr = Zipcode?.components(separatedBy: ",") ?? []
        self.zipcodetagvw.addTags(ziparr)
        selectedEditZipcodeList = ziparr

        
        let Skill = editdict["Skill"] as? String
        let skillarr = Skill?.components(separatedBy: ",") ?? []
        self.skilltagvw.addTags(skillarr)
        selectedEditSkillList = skillarr
        
        txtworkexp.text = editdict["WorkEx"] as? String
        
        txtreferencestudentname.text = editdict["Reference"] as? String
        txtreferencestudentcontactno.text = editdict["RefContactInfo"] as? String
        txtkeywordd.text = editdict["keywords"] as? String
        
        ResumePath = editdict["Resume"] as? String ?? ""
        
        StateID = editdict["StateID"] as? String ?? ""
        CityID = editdict["CityID"] as? String ?? ""
        QualificationID = editdict["StandardID"] as? String ?? ""
        myStateID = editdict["MyStateID"] as? String ?? ""
        myCityID = editdict["MyCityID"] as? String ?? ""
        
        self.CityApi()
        self.ZipcodeApi()
        
        self.myCityApi()
        self.myZipcodeApi()
    }
    
    func DropdownClick()  {
        
        txtclass.didSelect(completion: { (selected, index, id)  in
                                print(selected, index, id)
                                self.QualificationID = String(id)
                            })
        
        txtstate.didSelect(completion: { (selected, index, id)  in
            print(selected, index, id)
            self.StateID = String(id)
            self.txtcity.text = ""
            self.CityApi()
        })
        
        txtcity.didSelect(completion: { [self] (selected, index, id)  in
                   print(selected, index, id)
             self.myCityID = String(id)
            myzipcode.text = ""
            self.myZipcodeApi()
               })
      
        txtzipcode.didSelect(completion: { (selected, index, id)  in
                          print(selected, index, id)
            
            if self.isedit
           {
            if !self.selectedEditZipcodeList.contains(selected)
                  {
                    self.selectedEditZipcodeList.append(selected)
                    self.zipcodetagvw.addTag(selected)
                  
                  }
            }
            else
           {
            if self.selectedZipcodeList.count > 0
                  {
                  let dict:[String:Any] = self.ZipcodeList[index]
                      
                  let index1 = self.selectedZipcodeList.firstIndex(where: { dictionary in
                                                            guard let value = dictionary["ZipCode"] as? String
                                                              else { return false }
                                                            return value == dict["ZipCode"] as? String
                                                          })
                                                          if let index2 = index1 {
                                                         self.selectedZipcodeList.remove(at: index2)
                                                           self.selectedZipcodeList.append(dict)
                                                          }
                       else
                                                          {
                                                            self.selectedZipcodeList.append(dict)
                                                             self.zipcodetagvw.addTag(selected)
                       }
                  }
                  else
                  {
                      self.selectedZipcodeList.append(self.ZipcodeList[index])
                      self.zipcodetagvw.addTag(selected)
                  }
            }
      
                      })
        
        
        txtskill.didSelect(completion: { (selected, index, id)  in
                          print(selected, index, id)
            if self.isedit
            {
                if !self.selectedEditSkillList.contains(selected) {
                    self.selectedEditSkillList.append(selected)
                    self.skilltagvw.addTag(selected)
                }
               
            }
            else
            {
                if self.selectedSkillList.count > 0
                           {
                           let dict:[String:Any] = self.SkillList[index]
                               
                               let index1 = self.selectedSkillList.firstIndex(where: { dictionary in
                                                          guard let value = dictionary["Skill"] as? String
                                                            else { return false }
                                                          return value == dict["Skill"] as? String
                                                        })
                                                        if let index2 = index1 {
                                                       self.selectedSkillList.remove(at: index2)
                                                         self.selectedSkillList.append(dict)
                                                        }
                     else
                                                        {
                                                          self.selectedSkillList.append(dict)
                                                           self.skilltagvw.addTag(selected)
                     }
                          
                           }
                           else
                           {
                               self.selectedSkillList.append(self.SkillList[index])
                               self.skilltagvw.addTag(selected)
                           }
            }
           
             
                      })
        
        mystate.didSelect(completion: { (selected, index, id)  in
                   print(selected, index, id)
                   self.myStateID = String(id)
                   self.mycity.text = ""
                   self.myCityApi()
               })
               
        mycity.didSelect(completion: { [self] (selected, index, id)  in
                          print(selected, index, id)
                    self.CityID = String(id)
                   self.txtzipcode.text = ""
            self.zipcodetagvw.removeAllTags()
            selectedEditZipcodeList.removeAll()
            selectedZipcodeList.removeAll()
                   self.ZipcodeApi()
                      })
        
        
    }
    
    func SetupRadioButton()  {
           let chkarr = [chkmale,chkfemale]
             
             for chk in chkarr {
                 chk?.markType = .radio
                 chk?.boxType = .circle
                 chk?.tintColor = #colorLiteral(red: 0.3450980392, green: 0.6156862745, blue: 0.04705882353, alpha: 1)
                 chk?.secondaryTintColor = UIColor.lightGray
                 chk?.stateChangeAnimation = .fill
                // chk?.secondaryCheckmarkTintColor = UIColor.red
             }
             
      
    }
    
     func SetupTaglistview()  {
        zipcodetagvw.delegate = self
       
        skilltagvw.delegate = self
       // skilltagvw.addTags(["all", "your", "tag"])
        
        
        
    }
    
    // MARK: TagListViewDelegate
    
//    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
//        print("Tag pressed: \(title), \(sender)")
//        tagView.isSelected = !tagView.isSelected
//    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
        if isedit {
            if sender == skilltagvw
            {
                let index = selectedEditSkillList.firstIndex(of: title) ?? 0
                selectedEditSkillList.remove(at: index)
            }
            else
            {
                let index = selectedEditZipcodeList.firstIndex(of: title) ?? 0
                selectedEditZipcodeList.remove(at: index)
            }
        }
        else
        {
            if sender == skilltagvw {
                       
                       let index = selectedSkillList.firstIndex(where: { dictionary in
                                                                          guard let value = dictionary["Skill"] as? String
                                                                            else { return false }
                                                                          return value == title
                                                                        })
                                                                        if let index = index {
                                                                            selectedSkillList.remove(at: index)
                                                                        }
                   }
                   else
                   {
                       let index = selectedZipcodeList.firstIndex(where: { dictionary in
                                                                                     guard let value = dictionary["ZipCode"] as? String
                                                                                       else { return false }
                                                                                     return value == title
                                                                                   })
                                                                                   if let index = index {
                                                                                       selectedZipcodeList.remove(at: index)
                                                                                   }
                   }
        }
       
    }
    
     //MARK: - API
    
    func StateApi()
                        {
                            if !isInternetAvailable(){
                                noInternetConnectionAlert(uiview: self)
                            }
                            else
                            {
                           let parameters = [:] as [String : Any]
                             
                             let url = ServiceList.SERVICE_URL+ServiceList.STATELIST_API
                             
                                 callApi(url,
                                         method: .get,
                                         param: parameters ,
                                         withLoader: true)
                                       { (result) in
                                            print("\(ServiceList.STATELIST_API)==>RESPONSE:",result)
                                            if result.getBool(key: "status")
                                            {
                                                let dict = result["data"] as? [String:Any] ?? [:]
                                                self.StateList = dict.getArrayofDictionary(key: "state")
                                                self.txtstate.optionArray = self.StateList.compactMap {"\($0["Name"] as? String ?? "0")"}
                                                self.txtstate.optionIds =  self.StateList.compactMap {Int( $0["ID"] as? String ?? "0")}
                                                
                                                self.mystate.optionArray = self.StateList.compactMap {"\($0["Name"] as? String ?? "0")"}
                                                self.mystate.optionIds =  self.StateList.compactMap {Int( $0["ID"] as? String ?? "0")}
                                            }
                                         else
                                            {
                                             showToast(uiview: self, msg: result.getString(key: "message"))
                                         }
                                }
                            }
                            
                        }
    
    func CityApi()
                           {
                               if !isInternetAvailable(){
                                   noInternetConnectionAlert(uiview: self)
                               }
                               else
                               {
                                  
                              let parameters = ["StateID":StateID] as [String : Any]
                                
                                let url = ServiceList.SERVICE_URL+ServiceList.STATE_CITY_API
                                
                                    callApi(url,
                                            method: .post,
                                            param: parameters ,
                                            withLoader: true)
                                          { (result) in
                                               print("\(ServiceList.STATE_CITY_API)==>RESPONSE:",result)
                                               if result.getBool(key: "status")
                                               {
                                                   let dict = result["data"] as? [String:Any] ?? [:]
                                                   self.CityList = dict.getArrayofDictionary(key: "city")
                                                   self.txtcity.optionArray = self.CityList.compactMap {"\($0["Name"] as? String ?? "0")"}
                                                   self.txtcity.optionIds =  self.CityList.compactMap {Int( $0["ID"] as? String ?? "0")}
                                               }
                                            else
                                               {
                                                showToast(uiview: self, msg: result.getString(key: "message"))
                                            }
                                   }
                               }
                               
                           }
    
    func ZipcodeApi()
                             {
                                 if !isInternetAvailable(){
                                     noInternetConnectionAlert(uiview: self)
                                 }
                                 else
                                 {
                                    
                                let parameters = ["CityID": CityID] as [String : Any]
                                  
                                  let url = ServiceList.SERVICE_URL+ServiceList.STATE_ZIPCODE_API
                                  
                                      callApi(url,
                                              method: .post,
                                              param: parameters ,
                                              withLoader: true)
                                            { (result) in
                                                 print("\(ServiceList.STATE_ZIPCODE_API)==>RESPONSE:",result)
                                                 if result.getBool(key: "status")
                                                 {
                                                     let dict = result["data"] as? [String:Any] ?? [:]
                                                     self.ZipcodeList = dict.getArrayofDictionary(key: "Zipcode")
                                                    
//                                                    self.ZipcodeList.append(["Name": "light", "ID": "1"])
//                                                    self.ZipcodeList.append(["Name": "television", "ID": "2"])
//                                                    self.ZipcodeList.append(["Name": "mobile", "ID": "3"])
                                                    
                                                     self.txtzipcode.optionArray = self.ZipcodeList.compactMap {"\($0["ZipCode"] as? String ?? "0")"}
                                                     self.txtzipcode.optionIds =  self.ZipcodeList.compactMap {Int( $0["ZipcodeID"] as? String ?? "0")}
                                                 }
                                              else
                                                 {
                                                  showToast(uiview: self, msg: result.getString(key: "message"))
                                              }
                                     }
                                 }
                                 
                             }
    
     func myCityApi()
                               {
                                   if !isInternetAvailable(){
                                       noInternetConnectionAlert(uiview: self)
                                   }
                                   else
                                   {
                                      
                                  let parameters = ["StateID":myStateID] as [String : Any]
                                    
                                    let url = ServiceList.SERVICE_URL+ServiceList.STATE_CITY_API
                                    
                                        callApi(url,
                                                method: .post,
                                                param: parameters ,
                                                withLoader: true)
                                              { (result) in
                                                   print("\(ServiceList.STATE_CITY_API)==>RESPONSE:",result)
                                                   if result.getBool(key: "status")
                                                   {
                                                       let dict = result["data"] as? [String:Any] ?? [:]
                                                       let arr = dict.getArrayofDictionary(key: "city")
                                                       self.mycity.optionArray = arr.compactMap {"\($0["Name"] as? String ?? "0")"}
                                                       self.mycity.optionIds =  arr.compactMap {Int( $0["ID"] as? String ?? "0")}
                                                   }
                                                else
                                                   {
                                                    showToast(uiview: self, msg: result.getString(key: "message"))
                                                }
                                       }
                                   }
                                   
                               }
        
        func myZipcodeApi()
                                 {
                                     if !isInternetAvailable(){
                                         noInternetConnectionAlert(uiview: self)
                                     }
                                     else
                                     {
                                        
                                    let parameters = ["CityID":myCityID] as [String : Any]
                                      
                                      let url = ServiceList.SERVICE_URL+ServiceList.STATE_ZIPCODE_API
                                      
                                          callApi(url,
                                                  method: .post,
                                                  param: parameters ,
                                                  withLoader: true)
                                                { (result) in
                                                     print("\(ServiceList.STATE_ZIPCODE_API)==>RESPONSE:",result)
                                                     if result.getBool(key: "status")
                                                     {
                                                         let dict = result["data"] as? [String:Any] ?? [:]
                                                         let arr = dict.getArrayofDictionary(key: "Zipcode")

                                                         self.myzipcode.optionArray = arr.compactMap {"\($0["ZipCode"] as? String ?? "0")"}
                                                         self.myzipcode.optionIds =  arr.compactMap {Int( $0["ZipcodeID"] as? String ?? "0")}
                                                     }
                                                  else
                                                     {
                                                      showToast(uiview: self, msg: result.getString(key: "message"))
                                                  }
                                         }
                                     }
                                     
                                 }
    
    func SkillApi()
                                {
                                    if !isInternetAvailable(){
                                        noInternetConnectionAlert(uiview: self)
                                    }
                                    else
                                    {
                                       
                                   let parameters = ["CityID":CityID] as [String : Any]
                                     
                                     let url = ServiceList.SERVICE_URL+ServiceList.STATE_SKILL_LIST_API
                                     
                                         callApi(url,
                                                 method: .get,
                                                 param: parameters ,
                                                 withLoader: true)
                                               { (result) in
                                                    print("\(ServiceList.STATE_SKILL_LIST_API)==>RESPONSE:",result)
                                                    if result.getBool(key: "status")
                                                    {
                                                        let dict = result["data"] as? [String:Any] ?? [:]
                                                        self.SkillList = dict.getArrayofDictionary(key: "Skill")
                                                        
                                                        self.txtskill.optionArray = self.SkillList.compactMap {"\($0["Skill"] as? String ?? "0")"}
                                                        self.txtskill.optionIds =  self.SkillList.compactMap {Int( $0["SkillID"] as? String ?? "0")}
                                                    }
                                                 else
                                                    {
                                                     showToast(uiview: self, msg: result.getString(key: "message"))
                                                 }
                                        }
                                    }
                                    
                                }
    
    func StandardListApi()
                                     {
                                         if !isInternetAvailable(){
                                             noInternetConnectionAlert(uiview: self)
                                         }
                                         else
                                         {
                                            
                                        let parameters = ["ROLEID":"2"] as [String : Any]
                                          
                                          let url = ServiceList.SERVICE_URL+ServiceList.STATE_STANDARD_LIST_API
                                          
                                              callApi(url,
                                                      method: .post,
                                                      param: parameters ,
                                                      withLoader: true)
                                                    { (result) in
                                                         print("\(ServiceList.STATE_STANDARD_LIST_API)==>RESPONSE:",result)
                                                         if result.getBool(key: "status")
                                                         {
                                                             let dict = result["data"] as? [String:Any] ?? [:]
                                                            let arr = dict.getArrayofDictionary(key: "Standard")
                                                             
                                                             self.txtclass.optionArray = arr.compactMap {"\($0["Standard"] as? String ?? "0")"}
                                                             self.txtclass.optionIds =  arr.compactMap {Int( $0["StandardID"] as? String ?? "0")}
                                                         }
                                                      else
                                                         {
                                                          showToast(uiview: self, msg: result.getString(key: "message"))
                                                      }
                                             }
                                         }
                                         
                                     }
    
     func validation()
            {
                self.view.endEditing(true)
                if txtfname.text == ""
                {
                 showToast(uiview: self, msg: "Please enter First Name")
                }
                else if txtfname.text?.trimmingString().count ?? 0 < 3
                {
                 showToast(uiview: self, msg: "Please enter at least 3 character for First Name")
                }
                else if txtlname.text == ""
                    {
                        showToast(uiview: self, msg: "Please enter Last Name")
                    }
                else if txtlname.text?.trimmingString().count ?? 0 < 3 
                    {
                        showToast(uiview: self, msg: "Please enter at least 3 character for Last Name")
                    }
                    else if  txtcontactno.text?.count ?? 0 < 10
                                   {
                                   showToast(uiview: self, msg: "Please enter valid Mobile Number")
                                   }
                    else if !txtemail.isValidEmail()
                    {
                     showToast(uiview: self, msg: "Please enter EmailId")
                    }
                    else if txtdob.text == ""
                                   {
                                   showToast(uiview: self, msg: "Please select DOB")
                                   }
                    else if txtaddress.text == ""
                    {
                    showToast(uiview: self, msg: "Please enter Address")
                    }
                    else if txtstate.text == ""
                    {
                        showToast(uiview: self, msg: "Please select State")
                    }
                    else if txtcity.text == ""
                    {
                     showToast(uiview: self, msg: "Please select City")
                    }
//                    else if selectedZipcodeList.count == 0 && isedit == false
//                    {
//                     showToast(uiview: self, msg: "Please select Zipcode")
//                    }
//                    else if selectedEditZipcodeList.count == 0 && isedit == true
//                                       {
//                                        showToast(uiview: self, msg: "Please select Zipcode")
//                                       }
                    else if txtschool.text == ""
                    {
                     showToast(uiview: self, msg: "Please Enter School/College/Institute")
                    }
                    else if txtclass.text == ""
                    {
                    showToast(uiview: self, msg: "Please select current Class")
                    }
//                    else if txtreferencestudentname.text == ""
//                    {
//                    showToast(uiview: self, msg: "Please Enter Reference Student Name")
//                    }
//                    else if  txtreferencestudentcontactno.text?.count ?? 0 < 10
//                    {
//                    showToast(uiview: self, msg: "Please Enter Reference Student Contact Number")
//                    }
                    else if mystate.text == ""
                                      {
                                          showToast(uiview: self, msg: "Please select your State")
                                      }
                                      else if mycity.text == ""
                                      {
                                       showToast(uiview: self, msg: "Please select your City")
                                      }
                                      else if myzipcode.text == ""
                                      {
                                       showToast(uiview: self, msg: "Please select your Zipcode")
                                      }
//                    else if txtkeywordd.text == ""
//                                       {
//                                        showToast(uiview: self, msg: "Please Enter Keyword")
//                                       }
                    else if selectedSkillList.count == 0 && isedit == false
                                       {
                                        showToast(uiview: self, msg: "Please select Skill")
                                       }
                    else if selectedEditSkillList.count == 0 && isedit == true
                                                         {
                                                          showToast(uiview: self, msg: "Please select Skill")
                                                         }
                    else if txtworkexp.text == ""
                                       {
                                        showToast(uiview: self, msg: "Please select Work Experience")
                                       }
//                    else if ResumePath == ""
//                               {
//                               showToast(uiview: self, msg: "Please Select Resume")
//                               }
                else
                {
                    if isedit {
                        EditProfileApi()
                    }
                    else
                    {
                       RegisterSendOTPApi()
                    }
                    
                }
            }
    
    func RegisterSendOTPApi()
                     {
                         if !isInternetAvailable(){
                             noInternetConnectionAlert(uiview: self)
                         }
                         else
                         {
                             let skillidarr = self.selectedSkillList.compactMap {"\($0["Skill"] as? String ?? "0")"}
                             let zipcodeidarr = self.selectedZipcodeList.compactMap {"\($0["ZipCode"] as? String ?? "0")"}
                             
                             let strskill = skillidarr.joined(separator: ",")
                             let strzipcode = zipcodeidarr.joined(separator: ",")
                             
                      //    var parameters = [:] as [String : Any]
                        let  parameters = [
                            "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                            "player_id":StoredData.shared.playerId ?? "123",
                                    "Role_ID" : "2",
                                    "FName" : txtfname.text ?? "",
                                    "LName" : txtlname.text ?? "",
                                    "Dob" : txtdob.text ?? "",
                                    "Gender" :  strradio,
                                    "Address1" : txtaddress.text!,
                                    "CityID" :  CityID,
                                 "StateID" : StateID,
                                 "LastInstituteName" :  txtschool.text!,
                                 "Standard" :  QualificationID,
                                 "keywords" :  txtkeywordd.text ?? "",
                                 "Skill" :  strskill,
                                 "RefContactInfo" :  txtreferencestudentcontactno.text ?? "",
                                 "Reference" :  txtreferencestudentname.text ?? "",
                                 "WorkEx" :  txtworkexp.text!,
                                 "email" :  txtemail.text!,
                                 "mobile_no" :  txtcontactno.text!,
                                 "Resume" :  ResumePath,
                                  "Zipcode" :  strzipcode,
                                   "mystate" :  myStateID,
                                    "mycity" :  myCityID,
                                     "myzipcode" :   myzipcode.text!
                          ] as [String : Any]
                          
                          let url = ServiceList.SERVICE_URL+ServiceList.REGISTER_SEND_OTP_API
                          
                              callApi(url,
                                      method: .post,
                                      param: parameters ,
                                      withLoader: true)
                                    { (result) in
                                      print("\(ServiceList.REGISTER_SEND_OTP_API)==>RESPONSE:",result)
                                         if result.getBool(key: "status")
                                         {
                                             let dict = result.getDictionary(key: "data")
                                            let mobileno = dict.getString(key: "mobileno")
                                          //1. Create the alert controller.
                                          let alert = UIAlertController(title: "Enter OTP", message: "\(result.getString(key: "message")) To \(mobileno.prefix(4))XXXXXX", preferredStyle: .alert)

                                          //2. Add the text field. You can configure it however you need.
                                          alert.addTextField { (textField) in
                                              textField.text = ""
                                             textField.placeholder = "Enter OTP"
                                          }

                                          // 3. Grab the value from the text field, and print it when the user clicks OK.
                                          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                                            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                                            print("Text field: \(textField?.text ?? "")")
                                             
                                            
                                            if textField!.text != dict["otp"] as? String
                                            {
                                                showToast(uiview: self, msg: "Please Enter Valid OTP")
                                            }
                                            else
                                            {
                                                self.RegisterApi()
                                            }
                                            
                                          }))

                                          // 4. Present the alert.
                                          self.present(alert, animated: true, completion: nil)
                                         }
                                      else
                                         {
                                          showToast(uiview: self, msg: result.getString(key: "message"))
                                      }
                             }
                         }
                         
                     }
    
    func RegisterApi()
                    {
                        if !isInternetAvailable(){
                            noInternetConnectionAlert(uiview: self)
                        }
                        else
                        {
                            let skillidarr = self.selectedSkillList.compactMap {"\($0["Skill"] as? String ?? "0")"}
                             let zipcodeidarr = self.selectedZipcodeList.compactMap {"\($0["ZipCode"] as? String ?? "0")"}
                            
                            let strskill = skillidarr.joined(separator: ",")
                            let strzipcode = zipcodeidarr.joined(separator: ",")
                            
                     //    var parameters = [:] as [String : Any]
                       let  parameters = [
                        "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                        "player_id":StoredData.shared.playerId ?? "123",
                                "Role_ID" : "2",
                                "FName" : txtfname.text ?? "",
                                "LName" : txtlname.text ?? "",
                                "Dob" : txtdob.text ?? "",
                                "Gender" :  strradio,
                                "Address1" : txtaddress.text!,
                                "CityID" :  CityID,
                                "StateID" : StateID,
                                "LastInstituteName" :  txtschool.text!,
                                "Standard" :  QualificationID,
                                "keywords" :  txtkeywordd.text ?? "",
                                "Skill" :  strskill,
                                "RefContactInfo" :  txtreferencestudentcontactno.text ?? "",
                                "Reference" :  txtreferencestudentname.text ?? "",
                                "WorkEx" :  txtworkexp.text!,
                                "email" :  txtemail.text!,
                                "mobile_no" :  txtcontactno.text!,
                                "Resume" :  ResumePath,
                                "Zipcode" :  strzipcode,
                                "mystate" :  myStateID,
                                "mycity" :  myCityID,
                                "myzipcode" :   myzipcode.text!
                         ] as [String : Any]
                         
                         let url = ServiceList.SERVICE_URL+ServiceList.APP_REGISTER_API
                         
                             callApi(url,
                                     method: .post,
                                     param: parameters ,
                                     withLoader: true)
                                   { (result) in
                                     print("\(ServiceList.APP_REGISTER_API)==>RESPONSE:",result)
                                        if result.getBool(key: "status")
                                        {
                                            let dict = result.getDictionary(key: "data")
                                            var dictuser = dict.getDictionary(key: "user")
                                            for (key, value) in dictuser {
                                                let val : NSObject = value as! NSObject;
                                                dictuser[key] = val.isEqual(NSNull()) ? "" : value
                                            }
                                            print(dictuser)
                                            UserDefaults.standard.setUserDict(value: dictuser)
                                            UserDefaults.standard.setIsLogin(value: true)
                                            UserDefaults.standard.set(dict.getString(key: "login_token"), forKey: "login_token")
                                            
                                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentDashbboardViewController") as! StudentDashbboardViewController
                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                            showToast(uiview: self, msg: result.getString(key: "message"))
                                        }
                                     else
                                        {
                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                     }
                            }
                        }
                        
                    }
    
    func EditProfileApi()
                      {
                          if !isInternetAvailable(){
                              noInternetConnectionAlert(uiview: self)
                          }
                          else
                          {
                            
                            let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                                                     "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                                                     ] as [String : Any]
                              
                              let strskill = selectedEditSkillList.joined(separator: ",")
                              let strzipcode = selectedEditZipcodeList.joined(separator: ",")
                              
                       //    var parameters = [:] as [String : Any]
                         let  parameters = [
                          "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                                  "Role_ID" : "2",
                                  "FName" : txtfname.text ?? "",
                                  "LName" : txtlname.text ?? "",
                                  "Dob" : txtdob.text ?? "",
                                  "Gender" :  strradio,
                                  "Address1" : txtaddress.text!,
                                  "CityID" :  CityID,
                                  "StateID" : StateID,
                                  "LastInstituteName" :  txtschool.text!,
                                  "Standard" :  QualificationID,
                                  "keywords" :  txtkeywordd.text ?? "",
                                  "Skill" :  strskill,
                                  "RefContactInfo" :  txtreferencestudentcontactno.text ?? "",
                                  "Reference" :  txtreferencestudentname.text ?? "",
                                  "WorkEx" :  txtworkexp.text!,
                                  "email" :  txtemail.text!,
                                  "mobile_no" :  txtcontactno.text!,
                                  "Resume" :  ResumePath,
                                  "Zipcode" :  strzipcode,
                                  "mystate" :  myStateID,
                                  "mycity" :  myCityID,
                                  "myzipcode" :   myzipcode.text!
                           ] as [String : Any]
                            
                            let imgdata = imgprofile?.image?.jpegData(compressionQuality: 0.5)
                           
                           let url = ServiceList.SERVICE_URL+ServiceList.EDIT_PROFILE_API
                           
                               callApi(url,
                                       method: .post,
                                       param: parameters ,
                                       extraHeader: header,
                                       withLoader: true,
                                       data: [imgdata!],
                                       dataKey: ["ProfileImage"])
                                     { (result) in
                                       print("\(ServiceList.APP_REGISTER_API)==>RESPONSE:",result)
                                          if result.getBool(key: "status")
                                          {
                                              let dict = result.getDictionary(key: "data")
                                              var dictuser = dict.getDictionary(key: "profile")
                                              for (key, value) in dictuser {
                                                  let val : NSObject = value as! NSObject;
                                                  dictuser[key] = val.isEqual(NSNull()) ? "" : value
                                              }
                                              print(dictuser)
                                              UserDefaults.standard.setUserDict(value: dictuser)
                                            //  UserDefaults.standard.setIsLogin(value: true)
                                             // UserDefaults.standard.set(dict.getString(key: "login_token"), forKey: "login_token")
                                              
                                              let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentDashbboardViewController") as! StudentDashbboardViewController
                                              self.navigationController?.pushViewController(nextViewController, animated: true)
                                            showToast(uiview: self, msg: result.getString(key: "message"))
                                          }
                                       else
                                          {
                                           showToast(uiview: self, msg: result.getString(key: "message"))
                                       }
                              }
                          }
                          
                      }
    
    
    
     //MARK: - Radio Button Action
    
    @IBAction func male_click(_ sender: M13Checkbox) {
        chkmale.checkState = .checked
        chkfemale.checkState = .unchecked
        strradio = "Male"
    }
    
    @IBAction func female_click(_ sender: M13Checkbox) {
        chkmale.checkState = .unchecked
        chkfemale.checkState = .checked
        strradio = "Female"
    }
    

    //MARK: - Button Action

    @IBAction func DOB_click(_ sender: UIButton) {
        
        self.view.endEditing(true)
          DatePickerPopover(title: "Select DOB")
          .setDateMode(.date)
          .setMaximumDate(Date())
          .setSelectedDate(Date())
          .setDoneButton(action: {
              popover, selectedDate in
              
              print("selectedDate \(selectedDate)")
              let formatter = DateFormatter()
                  formatter.dateFormat = "dd-MM-yyyy"
              
              let age = calcAge(birthday: formatter.string(from: selectedDate))
              if age > 18
              {
                  self.txtdob.text = formatter.string(from: selectedDate)
              }
              else
              {
                  DispatchQueue.main.async {
                      showToast(uiview: self, msg: "Age must be 18 year old")
                  }
              }
          })
          .setCancelButton(action: { _, _ in print("cancel")})
          .appear(originView: sender, baseViewController: self)
    }
    
    @IBAction func resume_click(_ sender: UIButton) {
        
        /*  let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive", "public.composite-content", "public.text"], in: .import) */

           let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String], in: .import)

           documentPicker.delegate = self
           present(documentPicker, animated: true, completion: nil)
    }
    
    @IBAction func back_click(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
    
    @IBAction func login_click(_ sender: UIButton) {
      let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func submit_click(_ sender: AnimatableButton) {
          validation()
    }
    @IBAction func profile_click(_ sender: AnimatableButton) {
        
        self.view.endEditing(true)
               openImageVideoSelectionPicker(picker: picker)
    }
    
    //MARK:- Image Picker
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
           if let image = info[.originalImage] as? UIImage {
               self.imgprofile.contentMode = .scaleAspectFill
               self.imgprofile.layer.masksToBounds = true
               self.imgprofile.image = image
           }
           
           self.dismiss(animated: true, completion: { () -> Void in

           })
       }
    
}

extension StudentRegistrationViewController: UIDocumentPickerDelegate{

     func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {

               let cico = url as URL
               print(cico)
               print(url)

               print(url.lastPathComponent)

               print(url.pathExtension)
        txtresume.text = url.lastPathComponent
        ResumePath = "\(url)"
        showToast(uiview: self, msg: "Resume Upload Successfully")
              }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
           controller.dismiss(animated: true, completion: nil)
       }
  }
