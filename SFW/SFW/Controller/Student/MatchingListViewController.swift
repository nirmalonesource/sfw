//
//  MatchingListViewController.swift
//  SFW
//
//  Created by My Mac on 19/11/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SideMenu

class MatchingListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tbllist: UITableView!
    @IBOutlet var lbltitle: UILabel!
    
  
    var companylist = [[String:Any]]()
    var SkillID = String()
    var CityID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if CityID != "" {
            lbltitle.text = "CITY MATCHING LIST"
        }
        if SkillID != "" {
            lbltitle.text = "SKILL MATCHING LIST"
        }
        StudentDashboardApi()
    }
    
     //MARK: - Tableview Data Source
                 
                 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                  
                    return companylist.count
                    
                 }
                 
                 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                  
                     let cell = tableView.dequeueReusableCell(withIdentifier: "Studentdashboardcell", for: indexPath) as! Studentdashboardcell
                    let dict:NSDictionary = companylist[indexPath.row] as NSDictionary
                    
                    cell.lblnumber.text = "# \(dict["JobCode"] as? String ?? "")"
                    cell.lblpostname.text = dict["JobTittle"] as? String
                    cell.lblcmpname.text = dict["CompanyName"] as? String
                    cell.lblexp.text = dict["Work_experience"] as? String
                    cell.lbllocation.text = dict["City_Name"] as? String
                    cell.lblrequirement.text = dict["SkillIds"] as? String
                    cell.lblqualification.text = dict["Qualification"] as? String
                    
            
                     return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let dict:NSDictionary = companylist[indexPath.row] as NSDictionary
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "JobDetailViewController") as! JobDetailViewController
        nextViewController.JobID = dict["JobID"] as! String
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
     //MARK: - Button Action
    
    @IBAction func menu_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

    
   
    
    
    // MARK: - API
    
   
    
    
           
             func StudentDashboardApi()
                                    {
                                        if !isInternetAvailable(){
                                            noInternetConnectionAlert(uiview: self)
                                        }
                                        else
                                        {
                                           let url = ServiceList.SERVICE_URL+ServiceList.JOB_DASHBOARD_API
                                          
                                          let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                     "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                     ] as [String : Any]
                                          
                                            let parameters = [ "CityID":CityID,
                                                               "Skill":SkillID,
                                                               "ZipCode":"",
                                                               "keywords":""
                                            ] as [String : Any]
                          
                                             callApi(url,
                                                           method: .post,
                                                           param: parameters ,
                                                           extraHeader: header,
                                                          withLoader: true)
                                                   { (result) in
                                                         print("\(ServiceList.JOB_DASHBOARD_API)==>RESPONSE:",result)
                                                       
                                                        if result.getBool(key: "status")
                                                        {
                                                            let res = result.strippingNulls()
                                                            let data = res["data"] as? [String : Any]
                                                            
                                                            self.companylist = data?["Job"] as? [[String : Any]] ?? []
                                                            
                                                           self.tbllist.reloadData()
                                                        }
                                                     else
                                                     {
                                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                                     }
                                            }
                                        }
                                        
                                    }
    

}
