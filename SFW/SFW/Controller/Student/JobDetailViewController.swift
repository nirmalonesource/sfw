//
//  JobDetailViewController.swift
//  SFW
//
//  Created by My Mac on 28/09/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable

class JobDetailViewController: UIViewController {
    
    @IBOutlet var lbljobcode: UILabel!
    @IBOutlet var lblpostname: UILabel!
      @IBOutlet var lblcmpname: UILabel!
      @IBOutlet var lblexp: UILabel!
      @IBOutlet var lbllocation: UILabel!
      @IBOutlet var lblrequirement: UILabel!
      @IBOutlet var lbltime: UILabel!
    
    @IBOutlet var lblqualification: UILabel!
    @IBOutlet var lblduration: UILabel!
    @IBOutlet var lbljobtiming: UILabel!
    @IBOutlet var lblstartdt: UILabel!
     @IBOutlet var lblenddt: UILabel!
     @IBOutlet var lblopenfor: UILabel!
     @IBOutlet var lblpayscale: UILabel!
     @IBOutlet var lblpaymentmode: UILabel!
     @IBOutlet var lblincentives: UILabel!
     @IBOutlet var lblmono: UILabel!
     @IBOutlet var lblemail: UILabel!
     @IBOutlet var lbladdress: UILabel!
     @IBOutlet var lblcontactby: UILabel!

    
    @IBOutlet var textvwdescr: UITextView!
    
    @IBOutlet var btnlike: AnimatableButton!
    @IBOutlet var btnapply: AnimatableButton!
    @IBOutlet var lbltitle: UILabel!
    
    var JobID = String()
    var jobdata = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        JobDetailApi()
    }
    
    func setdata()  {
        
        lbljobcode.text = "# \(jobdata["JobCode"] as? String ?? "")"
        lbltitle.text = jobdata["JobTittle"] as? String
        
        lblpostname.text = jobdata["JobTittle"] as? String
        lblcmpname.text = jobdata["CompanyName"] as? String
        lblexp.text = jobdata["Work_experience"] as? String
        lbllocation.text = jobdata["City_Name"] as? String
        lblrequirement.text = jobdata["SkillIds"] as? String
        
        lblqualification.text = jobdata["Qualification"] as? String
        lblduration.text = jobdata["Duration"] as? String
        lbljobtiming.text = jobdata["JobTiming"] as? String
        lblstartdt.text = jobdata["StartDate"] as? String
        lblenddt.text = jobdata["EndDate"] as? String
        lblopenfor.text = jobdata["OpenFor"] as? String
        lblpayscale.text = "\(jobdata["PayScale"] as? String ?? "") \((jobdata["PaymentSchedule"] as? String ?? ""))"
        lblpaymentmode.text = jobdata["PayMode"] as? String
        lblincentives.text = jobdata["AdditionalIncentives"] as? String
        lblmono.text = jobdata["PhoneContact"] as? String
        lblemail.text = jobdata["EmailIDs"] as? String
        lbladdress.text = jobdata["Address1"] as? String
        lblcontactby.text = jobdata["Candidates_Contact"] as? String
        
        textvwdescr.text = jobdata["Description"] as? String
        lblqualification.text = jobdata["Qualification"] as? String
        
        let likes = jobdata["likes"] as? String
        let apply = jobdata["apply"] as? String
        
        if likes == "1" {
            btnlike.setTitle("DISLIKE", for: .normal)
        }
        else
        {
            btnlike.setTitle("LIKE", for: .normal)
        }
        
        if apply == "1" {
            btnapply.setTitle("CANCEL", for: .normal)
        }
        else
        {
            btnapply.setTitle("APPLY", for: .normal)
        }
        
        let tapemail = UITapGestureRecognizer(target: self, action: #selector(JobDetailViewController.tapEmail))
        lblemail.isUserInteractionEnabled = true
        lblemail.addGestureRecognizer(tapemail)
        
       let tap = UITapGestureRecognizer(target: self, action: #selector(JobDetailViewController.tapFunction))
       lblmono.isUserInteractionEnabled = true
        lblmono.addGestureRecognizer(tap)
        
    }
    
    @objc
       func tapEmail(sender:UITapGestureRecognizer) {
           
        let lbl = sender.view as! UILabel
        print("tap working",lbl.text ?? "")
        let email = lbl.text ?? ""
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
       }
    
    @objc
       func tapFunction(sender:UITapGestureRecognizer) {
           
        let lbl = sender.view as! UILabel
        print("tap working",lbl.text ?? "")
       // let dict:NSDictionary = companylist[sender.tag] as NSDictionary
        callNumber(phoneNumber: lbl.text ?? "")
       }
    
    private func callNumber(phoneNumber:String) {

        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {

            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                     application.openURL(phoneCallURL as URL)

                }
            }
        }
    }
    
//MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
             self.navigationController?.popViewController(animated: true)
         }
    
    @IBAction func like_click(_ sender: AnimatableButton) {
        
        if btnlike.titleLabel?.text == "LIKE" {
                JobLikeDislikeApi(islike: "1")
        }
        else
        {
                JobLikeDislikeApi(islike: "0")
        }
               
        
    }
    @IBAction func apply_click(_ sender: AnimatableButton) {
           if btnapply.titleLabel?.text == "APPLY" {
                    JobApplyApi(isapply: "1")
            }
            else
            {
                    JobApplyApi(isapply: "0")
            }
    }
    
    
    // MARK: - API
             
               func JobDetailApi()
                                      {
                                          if !isInternetAvailable(){
                                              noInternetConnectionAlert(uiview: self)
                                          }
                                          else
                                          {
                                             let url = ServiceList.SERVICE_URL+ServiceList.JOB_DETAIL_API
                                            
                                            let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                       "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                       ] as [String : Any]
                                            
                                              let parameters = [ "JobID":JobID
                                              ] as [String : Any]
                            
                                               callApi(url,
                                                             method: .post,
                                                             param: parameters ,
                                                             extraHeader: header,
                                                            withLoader: true)
                                                     { (result) in
                                                           print("\(ServiceList.JOB_DETAIL_API)==>RESPONSE:",result)
                                                         
                                                          if result.getBool(key: "status")
                                                          {
                                                              let res = result.strippingNulls()
                                                              let data = res["data"] as? [String : Any]
                                                              
                                                            self.jobdata = data?["Job"] as! [String : Any]
                                                            self.setdata()
                                                          }
                                                       else
                                                       {
                                                           showToast(uiview: self, msg: result.getString(key: "message"))
                                                       }
                                              }
                                          }
                                          
                                      }
    
    func JobLikeDislikeApi(islike:String)
                                        {
                                            if !isInternetAvailable(){
                                                noInternetConnectionAlert(uiview: self)
                                            }
                                            else
                                            {
                                               let url = ServiceList.SERVICE_URL+ServiceList.JOB_LIKE_DISLIKE_API
                                              
                                              let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                         "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                         ] as [String : Any]
                                              
                                                let parameters = [ "JobID":JobID,
                                                                   "is_like":islike
                                                                   
                                                ] as [String : Any]
                              
                                                 callApi(url,
                                                               method: .post,
                                                               param: parameters ,
                                                               extraHeader: header,
                                                              withLoader: true)
                                                       { (result) in
                                                             print("\(ServiceList.JOB_LIKE_DISLIKE_API)==>RESPONSE:",result)
                                                           
                                                            if result.getBool(key: "status")
                                                            {
                                                                let res = result.strippingNulls()
                                                           //     let data = res["data"] as? [String : Any]
                                                                
                                                          //    self.jobdata["likes"] = res["is_like"]
                                                             // self.setdata()
                                                                
                                                                if res["is_like"] as! Int == 1 {
                                                                    self.btnlike.setTitle("DISLIKE", for: .normal)
                                                                }
                                                                else
                                                                {
                                                                    self.btnlike.setTitle("LIKE", for: .normal)
                                                                }
                                                                showToast(uiview: self, msg: result.getString(key: "message"))
                                                            }
                                                         else
                                                         {
                                                             showToast(uiview: self, msg: result.getString(key: "message"))
                                                         }
                                                }
                                            }
                                            
                                        }
    
    func JobApplyApi(isapply:String)
                                           {
                                               if !isInternetAvailable(){
                                                   noInternetConnectionAlert(uiview: self)
                                               }
                                               else
                                               {
                                                  let url = ServiceList.SERVICE_URL+ServiceList.JOB_APPLY_API
                                                 
                                                 let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                            "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                            ] as [String : Any]
                                                 
                                                   let parameters = [ "JobID":JobID,
                                                                      "is_apply":isapply
                                                                      
                                                   ] as [String : Any]
                                 
                                                    callApi(url,
                                                                  method: .post,
                                                                  param: parameters ,
                                                                  extraHeader: header,
                                                                 withLoader: true)
                                                          { (result) in
                                                                print("\(ServiceList.JOB_APPLY_API)==>RESPONSE:",result)
                                                              
                                                               if result.getBool(key: "status")
                                                               {
                                                                   let res = result.strippingNulls()
                                                              //     let data = res["data"] as? [String : Any]
                                                                   
                                                              //   self.jobdata["likes"] = res["is_like"]
                                                                // self.setdata()
                                                                   
                                                                   if res["is_apply"] as! Int == 1 {
                                                                       self.btnapply.setTitle("CANCEL", for: .normal)
                                                                   }
                                                                   else
                                                                   {
                                                                       self.btnapply.setTitle("APPLY", for: .normal)
                                                                   }
                                                                showToast(uiview: self, msg: result.getString(key: "message"))
                                                               }
                                                            else
                                                            {
                                                                showToast(uiview: self, msg: result.getString(key: "message"))
                                                            }
                                                   }
                                               }
                                               
                                           }
    
    

}
