//
//  StudentProfileViewController.swift
//  SFW
//
//  Created by My Mac on 30/09/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SideMenu
import IBAnimatable

class StudentProfileViewController: UIViewController {

    @IBOutlet var lblpostname: UILabel!
       @IBOutlet var lblcmpname: UILabel!
       @IBOutlet var lblexp: UILabel!
       @IBOutlet var lbllocation: UILabel!
       @IBOutlet var lblrequirement: UILabel!
    //   @IBOutlet var lbltime: UILabel!
          
       @IBOutlet var lblqualification: UILabel!
     //  @IBOutlet var lbljobtiming: UILabel!
       @IBOutlet var lblmono: UILabel!
       @IBOutlet var lblemail: UILabel!
    @IBOutlet var lblsex: UILabel!
    @IBOutlet var lbldob: UILabel!
    @IBOutlet var lbladdress: UILabel!
    
    @IBOutlet var lblschool: UILabel!
    @IBOutlet var lblclass: UILabel!
    
    @IBOutlet var lblzipcode: UILabel!
    @IBOutlet var lblmyzipcode: UILabel!
   
    @IBOutlet var imgprofile: AnimatableImageView!
    
    @IBOutlet weak var vwpdfheight: NSLayoutConstraint!
    
    var userdata = [String:Any]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        UserDetailApi()
    }
    
    func setdata()  {
//        btnprofile?.sd_setImage(with: URL(string: userdata["ProfileImage"] as? String ?? ""), for: .normal, placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
        imgprofile.sd_setImage(with: URL(string: userdata["ProfileImage"] as? String ?? ""), placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
        lblpostname.text = "\(userdata["FName"] as? String ?? "") \(userdata["LName"] as? String ?? "")"
        //  lblcmpname.text = userdata["CompanyName"] as? String
          lblexp.text = userdata["WorkEx"] as? String
          lbllocation.text = userdata["mycity"] as? String
          lblrequirement.text = userdata["Skill"] as? String
          
          lblqualification.text = userdata["Standard"] as? String
          lblmono.text = userdata["mobile_no"] as? String
          lblemail.text = userdata["email"] as? String
        lblsex.text = userdata["Gender"] as? String
        lbldob.text = userdata["Dob"] as? String
          lbladdress.text = userdata["Address1"] as? String
          
         // textvwdescr.text = userdata["Description"] as? String
          lblschool.text = userdata["LastInstituteName"] as? String
         lblclass.text = userdata["Standard"] as? String
          
        lblzipcode.text = userdata["Zipcode"] as? String
        lblmyzipcode.text = userdata["myzipcode"] as? String
        
        if userdata["Resume"] as! String == "" {
            vwpdfheight.constant = 0
        }
          
      }
    
    //MARK: - Button Action
    
    @IBAction func menu_click(_ sender: UIButton) {
                let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! SideMenuNavigationController
                       menu.presentationStyle.menuStartAlpha = 1.0
                       menu.presentationStyle.presentingEndAlpha = 0.5
                       present(menu, animated: true, completion: nil)
            }
    
    @IBAction func resume_click(_ sender: UIButton) {
        
//        let docController = UIDocumentInteractionController.init(url: URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(userdata["Resume"] as! String))
//                  docController.delegate = self
//                  docController.presentPreview(animated: true)
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        nextViewController.strlink = userdata["Resume"] as! String
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    @IBAction func edit_click(_ sender: AnimatableButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentRegistrationViewController") as! StudentRegistrationViewController
        nextViewController.isedit = true
        nextViewController.editdict = userdata
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    // MARK: - API
                
                  func UserDetailApi()
                                         {
                                             if !isInternetAvailable(){
                                                 noInternetConnectionAlert(uiview: self)
                                             }
                                             else
                                             {
                                                let url = ServiceList.SERVICE_URL+ServiceList.USER_DETAIL_API
                                               
                                               let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                          "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                          ] as [String : Any]
                                               
                                                 let parameters = ["Role_ID" :UserDefaults.standard.getUserDict()["Role_ID"] as? String ?? ""
                                                 ] as [String : Any]
                               
                                                  callApi(url,
                                                                method: .post,
                                                                param: parameters ,
                                                                extraHeader: header,
                                                               withLoader: true)
                                                        { (result) in
                                                              print("\(ServiceList.JOB_DETAIL_API)==>RESPONSE:",result)
                                                            
                                                             if result.getBool(key: "status")
                                                             {
                                                                 let res = result.strippingNulls()
                                                                 let data = res["data"] as? [String : Any]
                                                                 
                                                               self.userdata = data?["User"] as! [String : Any]
                                                               self.setdata()
                                                             }
                                                          else
                                                          {
                                                              showToast(uiview: self, msg: result.getString(key: "message"))
                                                          }
                                                 }
                                             }
                                             
                                         }
    
}


//extension StudentProfileViewController: UIDocumentPickerDelegate,UIDocumentInteractionControllerDelegate{
//
////   func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
////
////             let cico = url as URL
////             print(cico)
////             print(url)
////
////             print(url.lastPathComponent)
////
////             print(url.pathExtension)
////      txtresume.text = url.lastPathComponent
////      ResumePath = "\(url)"
////            }
//
//  func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
//         controller.dismiss(animated: true, completion: nil)
//     }
//    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
//        return self
//    }
//}
