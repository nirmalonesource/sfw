//
//  WebViewController.swift
//  SFW
//
//  Created by My Mac on 05/11/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController,WKNavigationDelegate {
    
    
    @IBOutlet var lbltitle: UILabel!
    var strlink = ""
    
    private let webView = WKWebView(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let url = URL(string: strlink)
//        lbltitle.text = url?.lastPathComponent
        lbltitle.text = "RESUME"
        self.webView.isOpaque = false
        self.webView.backgroundColor = UIColor.clear
        self.webView.scrollView.backgroundColor = UIColor.clear
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
           self.view.addSubview(self.webView)
        // You can set constant space for Left, Right, Top and Bottom Anchors
                            NSLayoutConstraint.activate([
                                self.webView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
                                self.webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
                                self.webView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
                                self.webView.topAnchor.constraint(equalTo: self.view.safeAreaTopAnchor, constant: 80)
                                ])
            // For constant height use the below constraint and set your height constant and remove either top or bottom constraint
            //self.webView.heightAnchor.constraint(equalToConstant: 200.0),

            self.view.setNeedsLayout()
        let request = URLRequest(url: URL(string: strlink) ?? NSURL() as URL)
            self.webView.load(request)
            self.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.webView.loadAnimation()
        self.stopAnimating()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.stopAnimating()
    }
    

    //MARK: - Button Action
       
       @IBAction func back_click(_ sender: UIButton) {
                self.navigationController?.popViewController(animated: true)
            }
}
