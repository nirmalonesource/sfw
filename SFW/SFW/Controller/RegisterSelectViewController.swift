//
//  RegisterSelectViewController.swift
//  SFW
//
//  Created by My Mac on 22/09/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IBAnimatable

class RegisterSelectViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

     //MARK: - Button Action
    @IBAction func back_click(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
    @IBAction func student_click(_ sender: AnimatableButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentRegistrationViewController") as! StudentRegistrationViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func company_click(_ sender: AnimatableButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyRegistrationViewController") as! CompanyRegistrationViewController
               self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func login_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
