//
//  SideMenuViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 26/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable

class sidemenutablecell: UITableViewCell {
    @IBOutlet var imgicon: UIImageView!
    @IBOutlet var lbltitle: UILabel!
    
   // @IBOutlet weak var btnarrow: UIImageView!
    
}

class SideMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
   
    @IBOutlet var btnprofile: AnimatableButton!
    @IBOutlet var lblusername: UILabel!
    @IBOutlet var lblemail: UILabel!
    @IBOutlet var tblsidemenu: UITableView!
  
    var menuarr = [String]()
    var menuimgarr = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      //  let cntr:ImageCropViewController = ImageCropViewController()
//               let image = UIImage(named: "picture")!
//               let controller: ImageCropViewController = ImageCropViewController(image: image)
             //  controller.delegate = self
        
       // cntr
        
        lblemail.text = UserDefaults.standard.getUserDict()["mobile_no"] as? String ?? ""
         btnprofile?.sd_setImage(with: URL(string: UserDefaults.standard.getUserDict()["ProfileImage"] as? String ?? ""), for: .normal, placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.getUserDict()["Role_ID"] as? String ?? "" == "3"
        {
            lblusername.text = "\(UserDefaults.standard.getUserDict()["CompanyName"] as? String ?? "")"
            menuarr = ["Dashboard","My Profile","Post Job","Student Search","Notification","Contact Us","Logout"]
            menuimgarr = ["Dashboard","Profile","Whislist","Search","Notification","contactus","Logout"]
        }
        else
        {
            lblusername.text = "\(UserDefaults.standard.getUserDict()["FName"] as? String ?? "") \(UserDefaults.standard.getUserDict()["LName"] as? String ?? "")"
            menuarr = ["Dashboard","My Profile","My Wishlist","My Applied Job","Notification","Contact Us","Logout"]
            menuimgarr = ["Dashboard","Profile","Whislist","jobapplication","Notification","contactus","Logout"]
        }
        
        tblsidemenu.reloadData()
    }
    
    
    //MARK: - Tableview Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuarr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sidemenutablecell", for: indexPath) as! sidemenutablecell
       tableView.tableFooterView = UIView()
        
         cell.lbltitle.text = menuarr[indexPath.row]
        let imageName = menuimgarr[indexPath.row]
        cell.imgicon.image = UIImage(named: imageName)
        
      //  cell.imgicon.changeImageViewImageColor(color: #colorLiteral(red: 0.3450980392, green: 0.6156862745, blue: 0.04705882353, alpha: 1))
       // cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example

        let currentCell = tableView.cellForRow(at: indexPath!) as! sidemenutablecell
      
                if currentCell.lbltitle.text == "Dashboard"
                {
                     if UserDefaults.standard.getUserDict()["Role_ID"] as? String ?? "" == "3"
                     {
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyDashboardViewController") as! CompanyDashboardViewController
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                    else
                     {
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentDashbboardViewController") as! StudentDashbboardViewController
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                   
                    // self.dismiss(animated: true, completion: nil)
                }
                    
                    else  if currentCell.lbltitle.text == "My Wishlist"
                    {
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "WishlistViewController") as! WishlistViewController
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }

                    else  if currentCell.lbltitle.text == "My Profile"
                    {
                        if UserDefaults.standard.getUserDict()["Role_ID"] as? String ?? "" == "3"
                         {
                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyProfileViewController") as! CompanyProfileViewController
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                        else
                         {
                           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentProfileViewController") as! StudentProfileViewController
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                        
                    }

                    else  if currentCell.lbltitle.text == "Post Job"
                    {
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "JobPostViewController") as! JobPostViewController
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                    
                    else  if currentCell.lbltitle.text == "Student Search"
                    {
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentSearchViewController") as! StudentSearchViewController
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
        
        else  if currentCell.lbltitle.text == "My Applied Job"
                           {
                               let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyJobViewController") as! MyJobViewController
                               self.navigationController?.pushViewController(nextViewController, animated: true)
                           }
                    
                    else  if currentCell.lbltitle.text == "Notification"
                               {
                                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                                   self.navigationController?.pushViewController(nextViewController, animated: true)
                               }
//
                else  if currentCell.lbltitle.text == "Contact Us"
                {
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
//

                else if currentCell.lbltitle.text == "Logout"
                {
                let alertController = UIAlertController(title: "", message: "Are you sure you want to Logout?", preferredStyle:UIAlertController.Style.alert)
                        
                if let popoverController = alertController.popoverPresentationController {
                            popoverController.sourceView = self.view //to set the source of your alert
                            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
                            }

                        alertController.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.destructive)
                          { action -> Void in
                            // Put your code here
                            self.LogoutApi()
                          })
                        alertController.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel)
                        { action -> Void in
                          // Put your code here
                        })
                        self.present(alertController, animated: true, completion: nil)
                     }
               
        
        tableView.deselectRow(at: indexPath!, animated: true)
    }
    
    //MARK: - Button Action Method
    @IBAction func profile_click(_ sender: AnimatableButton) {
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
//        nextViewController.isedit = true
//        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
     //MARK: - Web Services
    
    func LogoutApi()
        {
                        if !isInternetAvailable(){
                            noInternetConnectionAlert(uiview: self)
                        }
                        else
                        {
                      let parameters = [
                                        "device_token" : UIDevice.current.identifierForVendor!.uuidString
                                       ] as [String : Any]
                           
                           let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                     "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                     ] as [String : Any]
                        
                            let url = ServiceList.SERVICE_URL+ServiceList.LOGOUT_API
                             callApi(url,
                                     method: .post,
                                     param: parameters ,
                                     extraHeader: header,
                                     withLoader: true)
                                   { (result) in
                                        print("LOGOUTRESPONSE:",result)
                                        if result.getBool(key: "status")
                                        {
                                         
                                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                    UserDefaults.standard.setIsLogin(value: false)
                                    UserDefaults.standard.removeObject(forKey: "items")
                                    //self.productlist = [[String:Any]]()
                                    GlobalVariables.globalarray = [[String:Any]]()
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                        }
                                     else
                                        {
                                         
                                            showToast(uiview: self, msg: result.getString(key: "message"))
                                     }
                            }
            }
                        
      }
    
}
