//
//  IntroViewController.swift
//  SFW
//
//  Created by My Mac on 21/11/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable

class PagingCell: UICollectionViewCell {
    
    @IBOutlet var img: UIImageView!
}


class IntroViewController: UIViewController,UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {

     @IBOutlet var outercollection: UICollectionView!
    @IBOutlet var pagecntrl: UIPageControl!
    @IBOutlet var btnstart: AnimatableButton!
    
     var arrImages = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        arrImages = ["launch1","launch2","launch3"]
         btnstart.isHidden = true
        pagecntrl.currentPage = 0
        pagecntrl.transform = CGAffineTransform(scaleX: 2, y: 2)
        
//        if #available(iOS 14.0, *) {
//            pagecntrl.currentPageIndicatorTintColor = .green // custom color
//            pagecntrl.pageIndicatorTintColor = UIColor.green.withAlphaComponent(0.3)
//                } else {
//                    // Fallback on earlier versions
//                    for index: Int in 0...3 {
//                        guard pagecntrl.subviews.count > index else { return }
//                        let dot: UIView = pagecntrl.subviews[index]
//                        dot.layer.cornerRadius = dot.frame.size.height / 2
//                        if index == pagecntrl.currentPage {
//                            dot.backgroundColor = .green
//                            dot.layer.borderWidth = 0
//                        } else {
//                            dot.backgroundColor = UIColor.clear
//                            dot.layer.borderColor = UIColor.green.cgColor
//                            dot.layer.borderWidth = 1
//                        }
//                    }
//        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
          let pageNumber = outercollection.currentPage
        pagecntrl.currentPage = pageNumber
                  if pageNumber == arrImages.count - 1 {
                                 btnstart.isHidden = false
                             }
                             else
                             {
                                 btnstart.isHidden = true
                             }
      
    }
    @IBAction func start_click(_ sender: AnimatableButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    //MARK: - CollectionView Data Source
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
         
            
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          
            return arrImages.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
         
                
                  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PagingCell", for: indexPath) as! PagingCell
                
               
           //     let imgURL = arrImages[indexPath.row]["ImageURL"] as? String ?? ""
             let imageName = arrImages[indexPath.row]
              cell.img.image = UIImage(named: imageName)
          
           //    cell.img.sd_setImage(with: URL(string:imgURL), placeholderImage:#imageLiteral(resourceName: "Logo_"), options:.progressiveLoad, completed: nil)
            
    //        let url = URL(string: imgURL)
    //
    //        SDWebImageManager.shared.loadImage(
    //             with: url,
    //             options: .continueInBackground, // or .highPriority
    //             progress: nil,
    //             completed: { [weak self] (image, data, error, cacheType, finished, url) in
    //                 guard let sself = self else { return }
    //
    //                 if let err = error {
    //                     // Do something with the error
    //                     return
    //                 }
    //
    //                 guard let img = image else {
    //                     // No image handle this error
    //                     return
    //                 }
    //
    //                 // Do something with image
    //                 let thumb1 = image?.resized(withPercentage: 0.9)
    //                 cell.img.image  = thumb1
    //             }
    //         )
                
                return cell
            
           
        }
            
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
               {
                    let noOfCellsInRow = 1

                                   let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

                                   let totalSpace = flowLayout.sectionInset.left
                                       + flowLayout.sectionInset.right
                                       + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

                                   let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
                    
                    let frame = outercollection.frame

                    return CGSize(width: size, height: Int(frame.size.height))
                
               
                
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          //   let cell = colnpaging.cellForItem(at: indexPath) as! PagingCell
          //  imageTapped(image: cell.img.image!)
            
          
        }

}
