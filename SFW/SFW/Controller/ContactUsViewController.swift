//
//  ContactUsViewController.swift
//  SFW
//
//  Created by My Mac on 19/11/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SideMenu

class ContactUsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK: - Button Action
      
        @IBAction func menu_click(_ sender: UIButton) {
                let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! SideMenuNavigationController
                      menu.presentationStyle.menuStartAlpha = 1.0
                      menu.presentationStyle.presentingEndAlpha = 0.5
                      present(menu, animated: true, completion: nil)
           }

}
