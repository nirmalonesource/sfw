//
//  LoginViewController.swift
//  SFW
//
//  Created by My Mac on 16/09/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IBAnimatable

class LoginViewController: UIViewController {

    @IBOutlet var txtmono: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
          
    //MARK: - API
           func LoginApi()
                 {
                     if !isInternetAvailable(){
                         noInternetConnectionAlert(uiview: self)
                     }
                     else
                     {
                      let parameters = ["mobile_no" : txtmono.text!,
                                        "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                                        "player_id":StoredData.shared.playerId ?? "123"
                             ] as [String : Any]
                        let url = ServiceList.SERVICE_URL+ServiceList.LOGIN_OTP_API
                          callApi(url,
                                        method: .post,
                                        param: parameters, withLoader: true)
                                { (result) in
                                     print("\(ServiceList.LOGIN_OTP_API)==>RESPONSE:",result)
                                     if result.getBool(key: "status")
                                     {
                                      let dict = result.getDictionary(key: "data")
                                      
                                      let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "VerificationViewController") as! VerificationViewController
                                        nextViewController.Dictdata = dict
                                      self.navigationController?.pushViewController(nextViewController, animated: true)
                                     }
                                  else
                                     {
                                      showToast(uiview: self, msg: result.getString(key: "message"))
                                  }
                         }
                     }
                     
                 }

    

   //MARK: - Button Action
    
    @IBAction func login_click(_ sender: AnimatableButton) {
        if  txtmono.text?.count ?? 0 < 10
        {
        showToast(uiview: self, msg: "Please enter valid Mobile Number")
        }
        else
        {
            LoginApi()
        }
    }
    
    @IBAction func account_click(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterSelectViewController") as! RegisterSelectViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    

}
