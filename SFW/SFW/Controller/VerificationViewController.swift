//
//  VerificationViewController.swift
//  SFW
//
//  Created by My Mac on 17/09/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IBAnimatable

class VerificationViewController: UIViewController {

    var Dictdata = [String:Any]()
    
    
    @IBOutlet var txtcode: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - API
              func CheckLoginApi()
                    {
                        if !isInternetAvailable(){
                            noInternetConnectionAlert(uiview: self)
                        }
                        else
                        {
                            let parameters = ["mobile_no" : Dictdata["mobileno"]!,
                                           "otp" : txtcode.text!,
                                             "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                                             "player_id":StoredData.shared.playerId ?? "123"
                                ] as [String : Any]
                           let url = ServiceList.SERVICE_URL+ServiceList.CHECK_LOGIN_OTP_API
                             callApi(url,
                                           method: .post,
                                           param: parameters, withLoader: true)
                                   { (result) in
                                        print("\(ServiceList.CHECK_LOGIN_OTP_API)==>RESPONSE:",result)
                                        if result.getBool(key: "status")
                                        {
                                         
                                         let dict = result.getDictionary(key: "data")
                                          var dictuser = dict.getDictionary(key: "user")
                                         for (key, value) in dictuser {
                                             let val : NSObject = value as! NSObject;
                                             dictuser[key] = val.isEqual(NSNull()) ? "" : value
                                         }
                                       //  dictuser["login_token"] = dict.getString(key: "login_token")
                                         print(dictuser)
                                         UserDefaults.standard.setUserDict(value: dictuser)
                                         UserDefaults.standard.setIsLogin(value: true)
                                         UserDefaults.standard.set(result.getString(key: "login_token"), forKey: "login_token")
                                            
                                            if UserDefaults.standard.getUserDict()["Role_ID"] as? String ?? "" == "3"
                                            {
                                               let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyDashboardViewController") as! CompanyDashboardViewController
                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                            }
                                            else
                                            {
                                               let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentDashbboardViewController") as! StudentDashbboardViewController
                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                            }
                                         
                                         
                                        }
                                     else
                                        {
                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                     }
                            }
                        }
                        
                    }

    
    
//MARK: - Button Action

  @IBAction func back_click(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
     }
    
    @IBAction func submit_click(_ sender: AnimatableButton) {
        if  !(txtcode.text!.count > 3)
               {
               showToast(uiview: self, msg: "Please enter valid OTP")
               }
               else
               {
                   CheckLoginApi()
               }
    }
    
    @IBAction func account_click(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentRegistrationViewController") as! StudentRegistrationViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
}
