//
//  CompanyMatchingListViewController.swift
//  SFW
//
//  Created by My Mac on 20/11/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SideMenu
import IBAnimatable

class CompanyMatchingListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tbllist: UITableView!
     var companylist = [[String:Any]]()
    var CityID = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        StudentDashboardApi()
    }
    
     //MARK: - Tableview Data Source
                 
                 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                  
                    return companylist.count
                    
                 }
                 
                 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                  
                     let cell = tableView.dequeueReusableCell(withIdentifier: "Studentdashboardcell", for: indexPath) as! Studentdashboardcell
                    let dict:NSDictionary = companylist[indexPath.row] as NSDictionary
                    
                   // cell.lblnumber.text = "# \(dict["JobCode"] as? String ?? "")"
                    cell.lblpostname.text = "\(dict["FName"] as? String ?? "") \((dict["LName"] as? String ?? ""))"
                    cell.lblcmpname.text = dict["mobile_no"] as? String
                    cell.lblexp.text = dict["WorkEx"] as? String
                    cell.lbllocation.text = dict["City_Name"] as? String
                    cell.lblrequirement.text = dict["Skill"] as? String
                    cell.lblqualification.text = dict["Standard"] as? String
                    
                    cell.lblemail.text = dict["email"] as? String
                    let tapemail = UITapGestureRecognizer(target: self, action: #selector(CompanyMatchingListViewController.tapEmail))
                    cell.lblemail.isUserInteractionEnabled = true
                    cell.lblemail.tag = indexPath.row
                    cell.lblemail.addGestureRecognizer(tapemail)
                    
                    
                    cell.btnpdf.tag = indexPath.row
                    cell.btnpdf.addTarget(self, action: #selector(self.BtnAction(_:)), for: .touchUpInside)
                    if dict["Resume"] as! String == "" {
                        cell.btnpdf.isHidden = true
                    }
                    else
                    {
                        cell.btnpdf.isHidden = false
                    }
                    
                                   let tap = UITapGestureRecognizer(target: self, action: #selector(CompanyMatchingListViewController.tapFunction))
                                   cell.lblcmpname.isUserInteractionEnabled = true
                                   cell.lblcmpname.tag = indexPath.row
                                   cell.lblcmpname.addGestureRecognizer(tap)
                     return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        let dict:NSDictionary = companylist[indexPath.row] as NSDictionary
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentDetailViewController") as! StudentDetailViewController
//        nextViewController.TitleStr = dict["JobTittle"] as! String
//        nextViewController.JobID = dict["JobID"] as! String
//        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc
       func tapEmail(sender:UITapGestureRecognizer) {
           
        let lbl = sender.view as! UILabel
        print("tap working",lbl.text ?? "")
        let email = lbl.text ?? ""
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
       }
    
    @objc
         func tapFunction(sender:UITapGestureRecognizer) {
             
          let lbl = sender.view as! UILabel
          print("tap working",lbl.text ?? "")
         // let dict:NSDictionary = companylist[sender.tag] as NSDictionary
          callNumber(phoneNumber: lbl.text ?? "")
         }
      
      private func callNumber(phoneNumber:String) {

          if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {

              let application:UIApplication = UIApplication.shared
              if (application.canOpenURL(phoneCallURL)) {
                  if #available(iOS 10.0, *) {
                      application.open(phoneCallURL, options: [:], completionHandler: nil)
                  } else {
                      // Fallback on earlier versions
                       application.openURL(phoneCallURL as URL)

                  }
              }
          }
      }
      
      @objc func BtnAction(_ sender: Any)
      {
          let btn = sender as? UIButton
          let dict:NSDictionary = companylist[btn!.tag] as NSDictionary
          
          let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
          nextViewController.strlink = dict["Resume"] as! String
          self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
     //MARK: - Button Action
    
    @IBAction func citywise_click(_ sender: UIButton) {
           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MatchingListViewController") as! MatchingListViewController
           nextViewController.CityID =  UserDefaults.standard.getUserDict()["CityID"] as? String ?? ""
           self.navigationController?.pushViewController(nextViewController, animated: true)
       }
    
    @IBAction func plus_click(_ sender: AnimatableButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "JobPostViewController") as! JobPostViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func menu_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func filter_click(_ sender: AnimatableButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentSearchViewController") as! StudentSearchViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
     // MARK: - API
           
             func StudentDashboardApi()
                                    {
                                        if !isInternetAvailable(){
                                            noInternetConnectionAlert(uiview: self)
                                        }
                                        else
                                        {
                                           let url = ServiceList.SERVICE_URL+ServiceList.COMPANY_FILTER_API
                                          
                                          let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                     "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                     ] as [String : Any]
                                          
                                            let parameters = [  "CityID":CityID
                                            ] as [String : Any]
                          
                                             callApi(url,
                                                           method: .post,
                                                           param: parameters ,
                                                           extraHeader: header,
                                                          withLoader: true)
                                                   { (result) in
                                                         print("\(ServiceList.COMPANY_FILTER_API)==>RESPONSE:",result)
                                                       
                                                        if result.getBool(key: "status")
                                                        {
                                                            let res = result.strippingNulls()
                                                            let data = res["data"] as? [String : Any]
                                                            
                                                            self.companylist = data?["user"] as? [[String : Any]] ?? []
                                                            
                                                            
                                                           self.tbllist.reloadData()
                                                        }
                                                     else
                                                     {
                                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                                     }
                                            }
                                        }
                                        
                                    }
    

}
