//
//  CompanyDashboardViewController.swift
//  SFW
//
//  Created by My Mac on 02/10/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SideMenu
import IBAnimatable
class CompanyDashboardViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tbllist: UITableView!
     @IBOutlet var lblcitycount: UILabel!
    @IBOutlet weak var vwnodata: UIView!
    @IBOutlet weak var nodataimg: UIImageView!
    @IBOutlet weak var lblnodata: UILabel!
     var companylist = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        StudentDashboardApi()
    }
    
     //MARK: - Tableview Data Source
                 
                 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                  
                    return companylist.count
                    
                 }
                 
                 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                  
                     let cell = tableView.dequeueReusableCell(withIdentifier: "Studentdashboardcell", for: indexPath) as! Studentdashboardcell
                    let dict:NSDictionary = companylist[indexPath.row] as NSDictionary
                    
                    cell.lblnumber.text = "# \(dict["JobCode"] as? String ?? "")"
                    cell.lblpostname.text = dict["JobTittle"] as? String
                    cell.lblcmpname.text = dict["CompanyName"] as? String
                    cell.lblexp.text = dict["Work_experience"] as? String
                    cell.lbllocation.text = dict["City_Name"] as? String
                    cell.lblrequirement.text = dict["SkillIds"] as? String
                    cell.lblqualification.text = dict["Qualification"] as? String
                    cell.lblcitycount.text = "\(dict["count"] as? String ?? "0")"
                     return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let dict:NSDictionary = companylist[indexPath.row] as NSDictionary
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentDetailViewController") as! StudentDetailViewController
        nextViewController.TitleStr = dict["JobTittle"] as! String
        nextViewController.JobID = dict["JobID"] as! String
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
     //MARK: - Button Action
    
    @IBAction func citywise_click(_ sender: UIButton) {
           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyMatchingListViewController") as! CompanyMatchingListViewController
           nextViewController.CityID =  UserDefaults.standard.getUserDict()["CityID"] as? String ?? ""
           self.navigationController?.pushViewController(nextViewController, animated: true)
       }
    
    @IBAction func plus_click(_ sender: AnimatableButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "JobPostViewController") as! JobPostViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func menu_click(_ sender: UIButton) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! SideMenuNavigationController
        menu.presentationStyle.menuStartAlpha = 1.0
        menu.presentationStyle.presentingEndAlpha = 0.5
        present(menu, animated: true, completion: nil)
    }
    @IBAction func filter_click(_ sender: AnimatableButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentSearchViewController") as! StudentSearchViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
     // MARK: - API
           
             func StudentDashboardApi()
                                    {
                                        if !isInternetAvailable(){
                                            noInternetConnectionAlert(uiview: self)
                                        }
                                        else
                                        {
                                           let url = ServiceList.SERVICE_URL+ServiceList.JOB_LIST_API
                                          
                                          let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                     "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                     ] as [String : Any]
                                          
                                            let parameters = [ :
                                            ] as [String : Any]
                          
                                             callApi(url,
                                                           method: .get,
                                                           param: parameters ,
                                                           extraHeader: header,
                                                          withLoader: true)
                                                   { (result) in
                                                         print("\(ServiceList.JOB_DASHBOARD_API)==>RESPONSE:",result)
                                                       
                                                        if result.getBool(key: "status")
                                                        {
                                                            let res = result.strippingNulls()
                                                            let data = res["data"] as? [String : Any]
                                                            
                                                            self.companylist = data?["Job_company"] as? [[String : Any]] ?? []
                                                             self.lblcitycount.text = "\(result.getInt(key: "CityCount"))"

                                                            if self.companylist.count > 0 {
                                                                self.tbllist.reloadData()
                                                                self.vwnodata.isHidden = true
                                                                self.tbllist.isHidden = false
                                                            }
                                                            else
                                                            {
                                                                self.tbllist.isHidden = true
                                                                self.vwnodata.isHidden = false
                                                                self.nodataimg.sd_setImage(with: URL(string: UserDefaults.standard.getUserDict()["ProfileImage"] as? String ?? ""), placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
                                                                self.lblnodata.text = result.getString(key: "message")
                                                            }
                                                            
                                                           
                                                        }
                                                     else
                                                     {
                                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                                     }
                                            }
                                        }
                                        
                                    }
    

}
