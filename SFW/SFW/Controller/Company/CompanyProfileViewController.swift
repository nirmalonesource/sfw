//
//  CompanyProfileViewController.swift
//  SFW
//
//  Created by My Mac on 02/10/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SideMenu
import IBAnimatable

class CompanyProfileViewController: UIViewController {

       @IBOutlet var lblcmpname: UILabel!
       @IBOutlet var lblincharge: UILabel!
    @IBOutlet var lblmono: UILabel!
     @IBOutlet var lblemail: UILabel!
       @IBOutlet var lblindustry: UILabel!
     @IBOutlet var lbladdress: UILabel!
       @IBOutlet var lblcity: UILabel!
       @IBOutlet var lblzipcode: UILabel!
    @IBOutlet var imgprofile: AnimatableImageView!
    
    
    var userdata = [String:Any]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        UserDetailApi()
    }
    
    func setdata()  {
         imgprofile.sd_setImage(with: URL(string: userdata["ProfileImage"] as? String ?? ""), placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
        lblcmpname.text = "\(userdata["CompanyName"] as? String ?? "")"
          lblincharge.text = userdata["PersonIncahrge"] as? String
         lblmono.text = userdata["mobile_no"] as? String
         lblemail.text = userdata["email"] as? String
          lblindustry.text = userdata["IndustryName"] as? String
        lbladdress.text = userdata["Address1"] as? String
          lblcity.text = userdata["City_Name"] as? String
        lblzipcode.text = userdata["Zipcode"] as? String
          
      }
    
    //MARK: - Button Action
    
    @IBAction func menu_click(_ sender: UIButton) {
                let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! SideMenuNavigationController
                       menu.presentationStyle.menuStartAlpha = 1.0
                       menu.presentationStyle.presentingEndAlpha = 0.5
                       present(menu, animated: true, completion: nil)
            }
    
  
    @IBAction func edit_click(_ sender: AnimatableButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyRegistrationViewController") as! CompanyRegistrationViewController
        nextViewController.isedit = true
        nextViewController.editdict = userdata
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    // MARK: - API
                
                  func UserDetailApi()
                                         {
                                             if !isInternetAvailable(){
                                                 noInternetConnectionAlert(uiview: self)
                                             }
                                             else
                                             {
                                                let url = ServiceList.SERVICE_URL+ServiceList.COMPANY_PROFILE_API
                                               
                                               let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                          "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                          ] as [String : Any]
                                               
                                                 let parameters = ["Role_ID" :UserDefaults.standard.getUserDict()["Role_ID"] as? String ?? ""
                                                 ] as [String : Any]
                               
                                                  callApi(url,
                                                                method: .post,
                                                                param: parameters ,
                                                                extraHeader: header,
                                                               withLoader: true)
                                                        { (result) in
                                                              print("\(ServiceList.JOB_DETAIL_API)==>RESPONSE:",result)
                                                            
                                                             if result.getBool(key: "status")
                                                             {
                                                                 let res = result.strippingNulls()
                                                                 let data = res["data"] as? [String : Any]
                                                                 
                                                               self.userdata = data?["User"] as! [String : Any]
                                                               self.setdata()
                                                             }
                                                          else
                                                          {
                                                              showToast(uiview: self, msg: result.getString(key: "message"))
                                                          }
                                                 }
                                             }
                                             
                                         }
    
}
