//
//  StudentDetailViewController.swift
//  SFW
//
//  Created by My Mac on 02/10/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable

class StudentDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet var lbltitle: UILabel!
     @IBOutlet var lbljobcode: UILabel!
    @IBOutlet var lblpostname: UILabel!
      @IBOutlet var lblcmpname: UILabel!
      @IBOutlet var lblexp: UILabel!
      @IBOutlet var lbllocation: UILabel!
      @IBOutlet var lblrequirement: UILabel!
    
    @IBOutlet var lblqualification: UILabel!
       @IBOutlet var lblduration: UILabel!
       @IBOutlet var lbljobtiming: UILabel!
       @IBOutlet var lblstartdt: UILabel!
        @IBOutlet var lblenddt: UILabel!
        @IBOutlet var lblopenfor: UILabel!
        @IBOutlet var lblpayscale: UILabel!
        @IBOutlet var lblpaymentmode: UILabel!
        @IBOutlet var lblincentives: UILabel!
        @IBOutlet var lblmono: UILabel!
        @IBOutlet var lblemail: UILabel!
        @IBOutlet var lbladdress: UILabel!
        @IBOutlet var lblcontactby: UILabel!
    
    @IBOutlet var textvwdescr: UITextView!
    
     @IBOutlet var tbllist: UITableView!
    @IBOutlet var tblheight: NSLayoutConstraint!
    @IBOutlet var swtchactive: UISwitch!
    
    var companylist = [[String:Any]]()
    var TitleStr = String()
    var JobID = String()
    var jobdata = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lbltitle.text = TitleStr
        JobDetailApi()
    }
    
//    override func updateViewConstraints() {
//        tblheight.constant = tbllist.contentSize.height
//        super.updateViewConstraints()
//    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
tblheight.constant = tbllist.contentSize.height
        tbllist.layoutIfNeeded()
    }
    
    func setdata()  {
        
        lbljobcode.text = "# \(jobdata["JobCode"] as? String ?? "")"
              lbltitle.text = jobdata["JobTittle"] as? String
              
              lblpostname.text = jobdata["JobTittle"] as? String
              lblcmpname.text = jobdata["CompanyName"] as? String
              lblexp.text = jobdata["Work_experience"] as? String
              lbllocation.text = jobdata["City_Name"] as? String
              lblrequirement.text = jobdata["SkillIds"] as? String
              
              lblqualification.text = jobdata["Qualification"] as? String
              lblduration.text = jobdata["Duration"] as? String
              lbljobtiming.text = jobdata["JobTiming"] as? String
              lblstartdt.text = jobdata["StartDate"] as? String
              lblenddt.text = jobdata["EndDate"] as? String
              lblopenfor.text = jobdata["OpenFor"] as? String
              lblpayscale.text = "\(jobdata["PayScale"] as? String ?? "") \((jobdata["PaymentSchedule"] as? String ?? ""))"
              lblpaymentmode.text = jobdata["PayMode"] as? String
              lblincentives.text = jobdata["AdditionalIncentives"] as? String
              lblmono.text = jobdata["PhoneContact"] as? String
              lblemail.text = jobdata["EmailIDs"] as? String
              lbladdress.text = jobdata["Address1"] as? String
              lblcontactby.text = jobdata["Candidates_Contact"] as? String
        
        textvwdescr.text = jobdata["Description"] as? String
        lblqualification.text = jobdata["Qualification"] as? String
        
        let isActive = jobdata["isActive"] as? String ?? "0"
        if isActive == "1" {
            swtchactive.isOn = true
        }
        else
        {
           swtchactive.isOn = false
        }
        
        let tapemail = UITapGestureRecognizer(target: self, action: #selector(StudentDetailViewController.tapEmail))
        lblemail.isUserInteractionEnabled = true
        lblemail.addGestureRecognizer(tapemail)
        
       let tap = UITapGestureRecognizer(target: self, action: #selector(StudentDetailViewController.tapFunction))
        lblmono.isUserInteractionEnabled = true
        lblmono.addGestureRecognizer(tap)
        
        
       
    }
    
    //MARK: - Tableview Data Source
                    
                    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                     
                       return companylist.count
                       
                    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
                    
                    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                     
                        let cell = tableView.dequeueReusableCell(withIdentifier: "Studentdashboardcell", for: indexPath) as! Studentdashboardcell
                       let dict:NSDictionary = companylist[indexPath.row] as NSDictionary
                       
                       cell.lblpostname.text = "\(dict["FName"] as? String ?? "") \(dict["LName"] as? String ?? "")"
                       cell.lblcmpname.text = dict["mobile_no"] as? String
                       cell.lblexp.text = dict["WorkEx"] as? String
                       cell.lbllocation.text = dict["City_Name"] as? String
                       cell.lblrequirement.text = dict["Skill"] as? String
                       cell.lblqualification.text = dict["Qualification"] as? String
                        
                        cell.lblemail.text = dict["email"] as? String
                        
                        
                                    cell.btnpdf.tag = indexPath.row
                                    cell.btnpdf.addTarget(self, action: #selector(self.BtnAction(_:)), for: .touchUpInside)
                        
                        if dict["Resume"] as! String == "" {
                            cell.btnpdf.isHidden = true
                        }
                        else
                        {
                            cell.btnpdf.isHidden = false
                        }
                        
                                    let tap = UITapGestureRecognizer(target: self, action: #selector(StudentDetailViewController.tapFunction))
                                    cell.lblcmpname.isUserInteractionEnabled = true
                                    cell.lblcmpname.tag = indexPath.row
                                    cell.lblcmpname.addGestureRecognizer(tap)
                        
                        let tapemail = UITapGestureRecognizer(target: self, action: #selector(StudentDetailViewController.tapEmail))
                        cell.lblemail.isUserInteractionEnabled = true
                        cell.lblemail.tag = indexPath.row
                        cell.lblemail.addGestureRecognizer(tapemail)
                   // tblheight.constant = tbllist.contentSize.height
                        
                        return cell
           }
    
    @objc
       func tapEmail(sender:UITapGestureRecognizer) {
           
        let lbl = sender.view as! UILabel
        print("tap working",lbl.text ?? "")
        let email = lbl.text ?? ""
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
       }
    
    @objc
       func tapFunction(sender:UITapGestureRecognizer) {
           
        let lbl = sender.view as! UILabel
        print("tap working",lbl.text ?? "")
       // let dict:NSDictionary = companylist[sender.tag] as NSDictionary
        callNumber(phoneNumber: lbl.text ?? "")
       }
    
    private func callNumber(phoneNumber:String) {

        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {

            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                     application.openURL(phoneCallURL as URL)

                }
            }
        }
    }

    
    @objc func BtnAction(_ sender: Any)
       {
           let btn = sender as? UIButton
           let dict:NSDictionary = companylist[btn!.tag] as NSDictionary
           
           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
           nextViewController.strlink = dict["Resume"] as! String
           self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
//MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
             self.navigationController?.popViewController(animated: true)
         }
    
    @IBAction func switchchanged(_ sender: UISwitch) {
        
        if sender.isOn {
            JobStatusApi(isactive: "1")
        }
        else
        {
            JobStatusApi(isactive: "0")
        }
    }
    
    @IBAction func edit_click(_ sender: AnimatableButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "JobPostViewController") as! JobPostViewController
        nextViewController.isedit = true
        nextViewController.editdict = jobdata
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    // MARK: - API
             
               func JobDetailApi()
                                      {
                                          if !isInternetAvailable(){
                                              noInternetConnectionAlert(uiview: self)
                                          }
                                          else
                                          {
                                             let url = ServiceList.SERVICE_URL+ServiceList.COMPANY_JOB_DETAIL_API
                                            
                                            let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                       "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                       ] as [String : Any]
                                            
                                              let parameters = [ "JobID":JobID
                                              ] as [String : Any]
                            
                                               callApi(url,
                                                             method: .post,
                                                             param: parameters ,
                                                             extraHeader: header,
                                                            withLoader: true)
                                                     { (result) in
                                                           print("\(ServiceList.JOB_DETAIL_API)==>RESPONSE:",result)
                                                         
                                                          if result.getBool(key: "status")
                                                          {
                                                              let res = result.strippingNulls()
                                                              let data = res["data"] as? [String : Any]
                                                              
                                                            self.jobdata = data?["Job_company_detail"] as! [String : Any]
                                                            self.companylist = data?["AppliedUser"] as? [[String : Any]] ?? []
                                                            self.setdata()
                                                            self.tbllist.reloadData()
                                                          }
                                                       else
                                                       {
                                                           showToast(uiview: self, msg: result.getString(key: "message"))
                                                       }
                                              }
                                          }
                                          
                                      }
    
    func JobStatusApi(isactive:String)
                                         {
                                             if !isInternetAvailable(){
                                                 noInternetConnectionAlert(uiview: self)
                                             }
                                             else
                                             {
                                                let url = ServiceList.SERVICE_URL+ServiceList.COMPANY_JOB_STATUS_API
                                               
                                               let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                          "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                          ] as [String : Any]
                                               
                                                 let parameters = [ "JobID":JobID,
                                                                     "isActive":isactive
                                                 ] as [String : Any]
                               
                                                  callApi(url,
                                                                method: .post,
                                                                param: parameters ,
                                                                extraHeader: header,
                                                               withLoader: true)
                                                        { (result) in
                                                              print("\(ServiceList.JOB_DETAIL_API)==>RESPONSE:",result)
                                                            
                                                             if result.getBool(key: "status")
                                                             {
                                                                 let res = result.strippingNulls()
                                                                 let data = res["data"] as? [String : Any]
                                                             showToast(uiview: self, msg: result.getString(key: "message"))
                                                             }
                                                          else
                                                          {
                                                              showToast(uiview: self, msg: result.getString(key: "message"))
                                                          }
                                                 }
                                             }
                                             
                                         }
       
    

}
