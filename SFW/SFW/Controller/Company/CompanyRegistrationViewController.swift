//
//  CompanyRegistrationViewController.swift
//  SFW
//
//  Created by My Mac on 21/09/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable
import SkyFloatingLabelTextField
import M13Checkbox
import iOSDropDown
import TagListView
import SwiftyPickerPopover
import MobileCoreServices

class CompanyRegistrationViewController: UIViewController,TagListViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet var txtcmpname: SkyFloatingLabelTextField!
    @IBOutlet var txtinchargename: SkyFloatingLabelTextField!
    @IBOutlet var txtcontactno: SkyFloatingLabelTextField!
    @IBOutlet var txtalternetcontactno: SkyFloatingLabelTextField!
    @IBOutlet var txtindustry: DropDown!
    @IBOutlet var txtaddress: SkyFloatingLabelTextField!
    @IBOutlet var txtcity: DropDown!
    @IBOutlet var txtstate: DropDown!
    @IBOutlet var txtzipcode: DropDown!
    // @IBOutlet var zipcodetagvw: TagListView!
    @IBOutlet var btnprofile: AnimatableButton!
    @IBOutlet var imglogo: UIImageView!
    @IBOutlet var imgprofile: UIImageView!
    @IBOutlet var btnsubmit: AnimatableButton!
    @IBOutlet var lblaccount: UILabel!
    @IBOutlet var btnlogin: UIButton!
    @IBOutlet var lblregister: UILabel!
    @IBOutlet var lblmobiletip: UILabel!
    
    
    var strradio = String()
    var StateList = [[String:Any]]()
    var CityList = [[String:Any]]()
    var ZipcodeList = [[String:Any]]()
    var SkillList = [[String:Any]]()
    var selectedZipcodeList = [[String:Any]]()
    var selectedSkillList = [[String:Any]]()
    var StateID = String()
    var CityID = String()
    var ResumePath = String()
    var IndustryId = String()
    var selectedEditZipcodeList = [String]()
    var editdict = [String:Any]()
    var isedit = false
    var picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        DropdownClick()
        //  SetupRadioButton()
        //  SetupTaglistview()
        
        StateApi()
        IndustryApi()
        lblmobiletip.text = ""
        if isedit {
            lblmobiletip.text = "*This is Primary Number using which All Communication carried out which cannot br Altered"
            lblregister.text = "EDIT PROFILE"
            btnsubmit.setTitle("UPDATE", for: .normal)
            btnprofile.isHidden = false
            imglogo.isHidden = true
            btnlogin.isHidden = true
            lblaccount.isHidden = true
            txtcontactno.isEnabled = false
            SetData()
        }
        
        picker.delegate = self
        
    }
    
    func SetData()  {
        
        imgprofile.sd_setImage(with: URL(string: editdict["ProfileImage"] as? String ?? ""), placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
        //  btnprofile?.sd_setImage(with: URL(string: editdict["ProfileImage"] as? String ?? ""), for: .normal, placeholderImage: UIImage(named:"Logo_"), options:.progressiveLoad, context: nil)
        txtcmpname.text = editdict["CompanyName"] as? String
        txtinchargename.text = editdict["PersonIncahrge"] as? String
        txtcontactno.text = editdict["mobile_no"] as? String
        txtalternetcontactno.text = editdict["email"] as? String
        
        txtindustry.text = editdict["IndustryName"] as? String
        txtaddress.text = editdict["Address1"] as? String
        txtstate.text = editdict["State_Name"] as? String
        txtcity.text = editdict["City_Name"] as? String
        txtzipcode.text = editdict["Zipcode"] as? String
        
        
        //   let Zipcode = editdict["Zipcode"] as? String
        // let ziparr = Zipcode?.components(separatedBy: ",") ?? []
        // self.zipcodetagvw.addTags(ziparr)
        //  selectedEditZipcodeList = ziparr
        //        for dict in ziparr.indices {
        //            selectedZipcodeList.append(ziparr[dict])
        //        }
        IndustryId = editdict["IndustryId"] as? String ?? ""
        StateID = editdict["StateID"] as? String ?? ""
        CityID = editdict["CityID"] as? String ?? ""
        
        self.CityApi()
        self.ZipcodeApi()
    }
    
    func DropdownClick()  {
        
        txtindustry.didSelect(completion: { (selected, index, id)  in
            print(selected, index, id)
            self.IndustryId = String(id)
        })
        
        txtstate.didSelect(completion: { (selected, index, id)  in
            print(selected, index, id)
            self.StateID = String(id)
            self.txtcity.text = ""
            self.CityApi()
        })
        
        txtcity.didSelect(completion: { (selected, index, id)  in
            print(selected, index, id)
            self.CityID = String(id)
            self.txtzipcode.text = ""
            //  self.zipcodetagvw.removeAllTags()
            self.ZipcodeApi()
        })
        
        txtzipcode.didSelect(completion: { (selected, index, id)  in
            print(selected, index, id)
        })
        
        txtzipcode.didSelect(completion: { (selected, index, id)  in
            print(selected, index, id)
        })
        
        
    }
    
    
    
    //     func SetupTaglistview()  {
    //        zipcodetagvw.delegate = self
    //
    //      //  skilltagvw.delegate = self
    //       // skilltagvw.addTags(["all", "your", "tag"])
    //
    //
    //
    //    }
    
    // MARK: TagListViewDelegate
    //    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
    //        print("Tag pressed: \(title), \(sender)")
    //        tagView.isSelected = !tagView.isSelected
    //    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
        
        if isedit {
            let index = selectedEditZipcodeList.firstIndex(of: title) ?? 0
            selectedEditZipcodeList.remove(at: index)
        }
        else
        {
            let index = selectedZipcodeList.firstIndex(where: { dictionary in
                guard let value = dictionary["ZipCode"] as? String
                else { return false }
                return value == title
            })
            if let index = index {
                selectedZipcodeList.remove(at: index)
            }
        }
        
        
        
    }
    
    //MARK: - API
    
    func IndustryApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            
            let parameters = [:] as [String : Any]
            
            let url = ServiceList.SERVICE_URL+ServiceList.INDUSTRY_LIST_API
            
            callApi(url,
                    method: .get,
                    param: parameters ,
                    withLoader: true)
            { (result) in
                print("\(ServiceList.INDUSTRY_LIST_API)==>RESPONSE:",result)
                if result.getBool(key: "status")
                {
                    let dict = result["data"] as? [String:Any] ?? [:]
                    let arrindus = dict.getArrayofDictionary(key: "Industry")
                    self.txtindustry.optionArray = arrindus.compactMap {"\($0["Industry"] as? String ?? "0")"}
                    self.txtindustry.optionIds =  arrindus.compactMap {Int( $0["IndustryID"] as? String ?? "0")}
                }
                else
                {
                    showToast(uiview: self, msg: result.getString(key: "message"))
                }
            }
        }
        
    }
    
    func StateApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            
            let parameters = [:] as [String : Any]
            
            let url = ServiceList.SERVICE_URL+ServiceList.STATELIST_API
            
            callApi(url,
                    method: .get,
                    param: parameters ,
                    withLoader: true)
            { (result) in
                print("\(ServiceList.STATELIST_API)==>RESPONSE:",result)
                if result.getBool(key: "status")
                {
                    let dict = result["data"] as? [String:Any] ?? [:]
                    self.StateList = dict.getArrayofDictionary(key: "state")
                    self.txtstate.optionArray = self.StateList.compactMap {"\($0["Name"] as? String ?? "0")"}
                    self.txtstate.optionIds =  self.StateList.compactMap {Int( $0["ID"] as? String ?? "0")}
                }
                else
                {
                    showToast(uiview: self, msg: result.getString(key: "message"))
                }
            }
        }
        
    }
    
    func CityApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            
            let parameters = ["StateID":StateID] as [String : Any]
            
            let url = ServiceList.SERVICE_URL+ServiceList.STATE_CITY_API
            
            callApi(url,
                    method: .post,
                    param: parameters ,
                    withLoader: true)
            { (result) in
                print("\(ServiceList.STATE_CITY_API)==>RESPONSE:",result)
                if result.getBool(key: "status")
                {
                    let dict = result["data"] as? [String:Any] ?? [:]
                    self.CityList = dict.getArrayofDictionary(key: "city")
                    self.txtcity.optionArray = self.CityList.compactMap {"\($0["Name"] as? String ?? "0")"}
                    self.txtcity.optionIds =  self.CityList.compactMap {Int( $0["ID"] as? String ?? "0")}
                }
                else
                {
                    showToast(uiview: self, msg: result.getString(key: "message"))
                }
            }
        }
        
    }
    
    func ZipcodeApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            
            let parameters = ["CityID":CityID] as [String : Any]
            
            let url = ServiceList.SERVICE_URL+ServiceList.STATE_ZIPCODE_API
            
            callApi(url,
                    method: .post,
                    param: parameters ,
                    withLoader: true)
            { (result) in
                print("\(ServiceList.STATE_ZIPCODE_API)==>RESPONSE:",result)
                if result.getBool(key: "status")
                {
                    let dict = result["data"] as? [String:Any] ?? [:]
                    self.ZipcodeList = dict.getArrayofDictionary(key: "Zipcode")
                    
                    //                                                    self.ZipcodeList.append(["Name": "light", "ID": "1"])
                    //                                                    self.ZipcodeList.append(["Name": "television", "ID": "2"])
                    //                                                    self.ZipcodeList.append(["Name": "mobile", "ID": "3"])
                    
                    self.txtzipcode.optionArray = self.ZipcodeList.compactMap {"\($0["ZipCode"] as? String ?? "0")"}
                    self.txtzipcode.optionIds =  self.ZipcodeList.compactMap {Int( $0["ZipcodeID"] as? String ?? "0")}
                }
                else
                {
                    showToast(uiview: self, msg: result.getString(key: "message"))
                }
            }
        }
        
    }
    
    
    
    func validation()
    {
        self.view.endEditing(true)
        if txtcmpname.text == ""
        {
            showToast(uiview: self, msg: "Please enter Company Name")
        }
        else if txtcmpname.text?.trimmingString().count ?? 0 < 3
        {
            showToast(uiview: self, msg: "Please enter at least 3 character for Company Name")
        }
        else if txtinchargename.text == ""
        {
            showToast(uiview: self, msg: "Please enter Contact Person Name")
        }
        else if txtinchargename.text?.trimmingString().count ?? 0 < 3
        {
            showToast(uiview: self, msg: "Please enter at least 3 character for Contact Person Name")
        }
        else if  txtcontactno.text?.count ?? 0 < 10
        {
            showToast(uiview: self, msg: "Please enter valid Mobile Number")
        }
        else if  !txtalternetcontactno.isValidEmail()
        {
            showToast(uiview: self, msg: "Please enter valid Email Address")
        }
        else if txtindustry.text == ""
        {
            showToast(uiview: self, msg: "Please enter Industry")
        }
        else if txtaddress.text == ""
        {
            showToast(uiview: self, msg: "Please enter Address")
        }
        else if txtstate.text == ""
        {
            showToast(uiview: self, msg: "Please select State")
        }
        else if txtcity.text == ""
        {
            showToast(uiview: self, msg: "Please select City")
        }
        else if txtzipcode.text == ""
        {
            showToast(uiview: self, msg: "Please select Zipcode")
        }
        //                else if selectedZipcodeList.count == 0
        //                {
        //                    showToast(uiview: self, msg: "Please select Zipcode")
        //                }
        //                    else if selectedSkillList.count == 0
        //                {
        //                        showToast(uiview: self, msg: "Please Enter Skill"
        //                    }
        
        else
        {
            if isedit {
                EditProfileApi()
            }
            else
            {
                RegisterSendOTPApi()
            }
            
        }
    }
    
    // MARK: - API
    
    func RegisterSendOTPApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            //  let skillidarr = self.selectedSkillList.compactMap {"\($0["Skill"] as? String ?? "0")"}
            //  let zipcodeidarr = self.selectedZipcodeList.compactMap {"\($0["ZipCode"] as? String ?? "0")"}
            
            //    let strskill = skillidarr.joined(separator: ",")
            //  let strzipcode = zipcodeidarr.joined(separator: ",")
            
            //    var parameters = [:] as [String : Any]
            let  parameters = [
                "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                "player_id":StoredData.shared.playerId ?? "123",
                "Role_ID" : "3",
                "CompanyName" : txtcmpname.text ?? "",
                "PersonIncahrge" : txtinchargename.text ?? "",
                "mobile_no" :  txtcontactno.text!,
                "email" :  txtalternetcontactno.text!,
                "IndustryId" :  IndustryId,
                "Address1" : txtaddress.text!,
                "CityID" :  CityID,
                "StateID" : StateID,
                "Zipcode" :  txtzipcode.text!
            ] as [String : Any]
            
            let url = ServiceList.SERVICE_URL+ServiceList.REGISTER_SEND_OTP_API
            
            callApi(url,
                    method: .post,
                    param: parameters ,
                    withLoader: true)
            { (result) in
                print("\(ServiceList.REGISTER_SEND_OTP_API)==>RESPONSE:",result)
                if result.getBool(key: "status")
                {
                    let dict = result.getDictionary(key: "data")
                    let mobileno = dict.getString(key: "mobileno")
                    //1. Create the alert controller.
                    let alert = UIAlertController(title: "Enter OTP", message: "\(result.getString(key: "message")) To \(mobileno.prefix(4))XXXXXX", preferredStyle: .alert)
                    
                    //2. Add the text field. You can configure it however you need.
                    alert.addTextField { (textField) in
                        textField.text = ""
                        textField.placeholder = "Enter OTP"
                    }
                    
                    // 3. Grab the value from the text field, and print it when the user clicks OK.
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                        let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                        print("Text field: \(textField?.text ?? "")")
                        
                        
                        if textField!.text != dict["otp"] as? String
                        {
                            showToast(uiview: self, msg: "Please Enter Valid OTP")
                        }
                        else
                        {
                            self.RegisterApi()
                        }
                        
                    }))
                    
                    // 4. Present the alert.
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    showToast(uiview: self, msg: result.getString(key: "message"))
                }
            }
        }
        
    }
    
    func RegisterApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            let  parameters = [
                "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                "player_id":StoredData.shared.playerId ?? "123",
                "Role_ID" : "3",
                "CompanyName" : txtcmpname.text ?? "",
                "PersonIncahrge" : txtinchargename.text ?? "",
                "mobile_no" :  txtcontactno.text!,
                "email" :  txtalternetcontactno.text!,
                "IndustryId" :  IndustryId,
                "Address1" : txtaddress.text!,
                "CityID" :  CityID,
                "StateID" : StateID,
                "Zipcode" :  txtzipcode.text!
            ] as [String : Any]
            
            let url = ServiceList.SERVICE_URL+ServiceList.APP_REGISTER_API
            
            callApi(url,
                    method: .post,
                    param: parameters ,
                    withLoader: true)
            { (result) in
                print("\(ServiceList.APP_REGISTER_API)==>RESPONSE:",result)
                if result.getBool(key: "status")
                {
                    let dict = result.getDictionary(key: "data")
                    var dictuser = dict.getDictionary(key: "user")
                    for (key, value) in dictuser {
                        let val : NSObject = value as! NSObject;
                        dictuser[key] = val.isEqual(NSNull()) ? "" : value
                    }
                    print(dictuser)
                    UserDefaults.standard.setUserDict(value: dictuser)
                    UserDefaults.standard.setIsLogin(value: true)
                    UserDefaults.standard.set(dict.getString(key: "login_token"), forKey: "login_token")
                    
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyDashboardViewController") as! CompanyDashboardViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                    
                }
                else
                {
                    showToast(uiview: self, msg: result.getString(key: "message"))
                }
            }
        }
        
    }
    
    
    func EditProfileApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
            ] as [String : Any]
            
            
            //    var parameters = [:] as [String : Any
            let  parameters = [
                "device_token" : UIDevice.current.identifierForVendor!.uuidString,
                "Role_ID" : "3",
                "CompanyName" : txtcmpname.text ?? "",
                "PersonIncahrge" : txtinchargename.text ?? "",
                "mobile_no" :  txtcontactno.text!,
                "email" :  txtalternetcontactno.text!,
                "IndustryId" :  IndustryId,
                "Address1" : txtaddress.text!,
                "CityID" :  CityID,
                "StateID" : StateID,
                "Zipcode" :  txtzipcode.text!
            ] as [String : Any]
            
            let imgdata = imgprofile?.image?.jpegData(compressionQuality: 0.5)
            
            let url = ServiceList.SERVICE_URL+ServiceList.EDIT_PROFILE_API
            
            callApi(url,
                    method: .post,
                    param: parameters ,
                    extraHeader: header,
                    withLoader: true,
                    data: [imgdata!],
                    dataKey: ["ProfileImage"])
            { (result) in
                print("\(ServiceList.APP_REGISTER_API)==>RESPONSE:",result)
                if result.getBool(key: "status")
                {
                    let dict = result.getDictionary(key: "data")
                    var dictuser = dict.getDictionary(key: "profile")
                    for (key, value) in dictuser {
                        let val : NSObject = value as! NSObject;
                        dictuser[key] = val.isEqual(NSNull()) ? "" : value
                    }
                    print(dictuser)
                    UserDefaults.standard.setUserDict(value: dictuser)
                    
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyDashboardViewController") as! CompanyDashboardViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                    showToast(uiview: self, msg: result.getString(key: "message"))
                }
                else
                {
                    showToast(uiview: self, msg: result.getString(key: "message"))
                }
            }
        }
        
    }
    
    
    
    
    //MARK: - Button Action
    
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func login_click(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func submit_click(_ sender: AnimatableButton) {
        validation()
    }
    @IBAction func profile_click(_ sender: AnimatableButton) {
        
        self.view.endEditing(true)
        openImageVideoSelectionPicker(picker: picker)
    }
    
    //MARK:- Image Picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.originalImage] as? UIImage {
            self.imgprofile.contentMode = .scaleAspectFill
            self.imgprofile.layer.masksToBounds = true
            self.imgprofile.image = image
        }
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
}

