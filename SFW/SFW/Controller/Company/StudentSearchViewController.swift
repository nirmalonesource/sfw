//
//  StudentSearchViewController.swift
//  SFW
//
//  Created by My Mac on 03/11/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import iOSDropDown
import IBAnimatable
import PDFKit

class StudentSearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var txtcity: DropDown!
    @IBOutlet var txtskill: DropDown!
    @IBOutlet var txtzipcode: UITextField!
    @IBOutlet var txtworkex: DropDown!
    
    @IBOutlet var tbllist: SelfSizedTableView!
    @IBOutlet var tblheight: NSLayoutConstraint!
    
    var companylist = [[String:Any]]()
    var SkillID = String()
    var CityID = String()
    var WorkEXList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SkillApi()
        CityApi()
        DropdownClick()
         CompanyFilterApi()
        WorkEXList = ["0-2 yrs","2-4 yrs","4-6 yrs","6-8 yrs","8-10 yrs","10+ yrs"]
        txtworkex.optionArray = WorkEXList
    }
    
    

   
   
    
    //MARK: - Tableview Data Source
             
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              
                return companylist.count
                
             }
             
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              
                 let cell = tableView.dequeueReusableCell(withIdentifier: "Studentdashboardcell", for: indexPath) as! Studentdashboardcell
                let dict:NSDictionary = companylist[indexPath.row] as NSDictionary
                
                cell.lblpostname.text = "\(dict["FName"] as? String ?? "") \(dict["LName"] as? String ?? "")"
                cell.lblcmpname.text = dict["mobile_no"] as? String
                cell.lblexp.text = dict["WorkEx"] as? String
                cell.lbllocation.text = dict["City_Name"] as? String
                cell.lblrequirement.text = dict["Skill"] as? String
                cell.lblqualification.text = dict["Standard"] as? String
                
                cell.lblemail.text = dict["email"] as? String
                let tapemail = UITapGestureRecognizer(target: self, action: #selector(StudentSearchViewController.tapEmail))
                cell.lblemail.isUserInteractionEnabled = true
                cell.lblemail.tag = indexPath.row
                cell.lblemail.addGestureRecognizer(tapemail)
                
                cell.layoutIfNeeded()
                cell.btnpdf.tag = indexPath.row
                cell.btnpdf.addTarget(self, action: #selector(self.BtnAction(_:)), for: .touchUpInside)
                if dict["Resume"] as! String == "" {
                    cell.btnpdf.isHidden = true
                }
                else
                {
                    cell.btnpdf.isHidden = false
                }
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(StudentSearchViewController.tapFunction))
                cell.lblcmpname.isUserInteractionEnabled = true
                cell.lblcmpname.tag = indexPath.row
                cell.lblcmpname.addGestureRecognizer(tap)
                // self.tblheight.constant = tableView.contentSize.height
                 return cell
    }
    @objc
       func tapEmail(sender:UITapGestureRecognizer) {
           
        let lbl = sender.view as! UILabel
        print("tap working",lbl.text ?? "")
        let email = lbl.text ?? ""
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
       }
    
    @objc
       func tapFunction(sender:UITapGestureRecognizer) {
           
        let lbl = sender.view as! UILabel
        print("tap working",lbl.text ?? "")
       // let dict:NSDictionary = companylist[sender.tag] as NSDictionary
        callNumber(phoneNumber: lbl.text ?? "")
       }
    
    private func callNumber(phoneNumber:String) {

        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {

            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                     application.openURL(phoneCallURL as URL)

                }
            }
        }
    }
    
    @objc func BtnAction(_ sender: Any)
    {
        let btn = sender as? UIButton
        let dict:NSDictionary = companylist[btn!.tag] as NSDictionary
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        nextViewController.strlink = dict["Resume"] as! String
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
//        let docController = UIDocumentInteractionController.init(url: URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(dict["Resume"] as! String))
//                         docController.delegate = self
//                         docController.presentPreview(animated: true)
        
//        if let path = Bundle.main.path(forResource: "sample", ofType: "pdf") {
//            if let pdfDocument = PDFDocument(url: URL(fileURLWithPath: path)) {
//                pdfView.displayMode = .singlePageContinuous
//                pdfView.autoScales = true
//                pdfView.displayDirection = .vertical
//                pdfView.document = pdfDocument
//            }
//        }
        
//        if #available(iOS 11.0, *) {
//            let pdfView = PDFView(frame: view.frame)
//             title = "Your_title_here"
//            if let url = URL(string: dict["Resume"] as! String),
//                           let pdfDocument = PDFDocument(url: url) {
//                           pdfView.displayMode = .singlePageContinuous
//                           pdfView.autoScales = true
//                           pdfView.displayDirection = .vertical
//                           pdfView.document = pdfDocument
//
//                           view.addSubview(pdfView)
//                       }
//        } else {
//            // Fallback on earlier versions
//        }
        
       
        
        

    }

  //MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
             self.navigationController?.popViewController(animated: true)
         }
    
    @IBAction func apply_click(_ sender: AnimatableButton) {
           CompanyFilterApi()
       }
       
       @IBAction func cleare_click(_ sender: AnimatableButton) {
           ClearFilter()
          CompanyFilterApi()
       }
    
    func ClearFilter()  {
           txtcity.text = ""
           txtskill.text = ""
           txtzipcode.text = ""
           txtworkex.text = ""
           SkillID = ""
           CityID = ""
           
       }
    
    func DropdownClick()  {
              
              txtskill.didSelect(completion: { (selected, index, id)  in
                                      print(selected, index, id)
                                      self.SkillID = String(selected)
                                  })
              
              txtcity.didSelect(completion: { (selected, index, id)  in
                         print(selected, index, id)
                   self.CityID = String(id)
                     })
       }
       
       
       // MARK: - API
       
       func CityApi()
                              {
                                  if !isInternetAvailable(){
                                      noInternetConnectionAlert(uiview: self)
                                  }
                                  else
                                  {
                                     
                                 let parameters = ["StateID":""] as [String : Any]
                                   
                                   let url = ServiceList.SERVICE_URL+ServiceList.STATE_CITY_API
                                   
                                       callApi(url,
                                               method: .post,
                                               param: parameters ,
                                               withLoader: true)
                                             { (result) in
                                                  print("\(ServiceList.STATE_CITY_API)==>RESPONSE:",result)
                                                  if result.getBool(key: "status")
                                                  {
                                                      let dict = result["data"] as? [String:Any] ?? [:]
                                                      let arr = dict.getArrayofDictionary(key: "city")
                                                      self.txtcity.optionArray = arr.compactMap {"\($0["Name"] as? String ?? "0")"}
                                                      self.txtcity.optionIds =  arr.compactMap {Int( $0["ID"] as? String ?? "0")}
                                                  }
                                               else
                                                  {
                                                   showToast(uiview: self, msg: result.getString(key: "message"))
                                               }
                                      }
                                  }
                                  
                              }
       
        func SkillApi()
                                       {
                                           if !isInternetAvailable(){
                                               noInternetConnectionAlert(uiview: self)
                                           }
                                           else
                                           {
                                              
                                          let parameters = ["CityID":""] as [String : Any]
                                            
                                            let url = ServiceList.SERVICE_URL+ServiceList.STATE_SKILL_LIST_API
                                            
                                                callApi(url,
                                                        method: .get,
                                                        param: parameters ,
                                                        withLoader: true)
                                                      { (result) in
                                                           print("\(ServiceList.STATE_SKILL_LIST_API)==>RESPONSE:",result)
                                                           if result.getBool(key: "status")
                                                           {
                                                               let dict = result["data"] as? [String:Any] ?? [:]
                                                               let arr = dict.getArrayofDictionary(key: "Skill")
                                                               
                                                               self.txtskill.optionArray = arr.compactMap {"\($0["Skill"] as? String ?? "0")"}
                                                               self.txtskill.optionIds =  arr.compactMap {Int( $0["SkillID"] as? String ?? "0")}
                                                           }
                                                        else
                                                           {
                                                            showToast(uiview: self, msg: result.getString(key: "message"))
                                                        }
                                               }
                                           }
                                           
                                       }
              
                func CompanyFilterApi()
                                       {
                                           if !isInternetAvailable(){
                                               noInternetConnectionAlert(uiview: self)
                                           }
                                           else
                                           {
                                              let url = ServiceList.SERVICE_URL+ServiceList.COMPANY_FILTER_API
                                             
                                             let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                        "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                        ] as [String : Any]
                                             
                                               let parameters = [ "CityID":CityID,
                                                                  "Skill":SkillID,
                                                                  "ZipCode":txtzipcode.text ?? "",
                                                                  "WorkEx":txtworkex.text ?? ""
                                               ] as [String : Any]
                             
                                                callApi(url,
                                                              method: .post,
                                                              param: parameters ,
                                                              extraHeader: header,
                                                             withLoader: true)
                                                      { (result) in
                                                            print("\(ServiceList.JOB_DASHBOARD_API)==>RESPONSE:",result)
                                                          
                                                           if result.getBool(key: "status")
                                                           {
                                                               let res = result.strippingNulls()
                                                               let data = res["data"] as? [String : Any]
                                                               
                                                               self.companylist = data?["user"] as? [[String : Any]] ?? []
                                                               
       //                                                        for dict in self.productlist {
       //                                                        self.temparr.append("")
       //                                                            self.pricetemparr.append("")
       //                                                        }
                                                            DispatchQueue.main.async {
                                                                self.tbllist.maxHeight = CGFloat(300 * (self.companylist.count))
                                                                self.tbllist.reloadData()
                                                            }
                                                              
                                                           }
                                                        else
                                                        {
                                                            showToast(uiview: self, msg: result.getString(key: "message"))
                                                        }
                                               }
                                           }
                                           
                                       }

}

//extension StudentSearchViewController: UIDocumentPickerDelegate,UIDocumentInteractionControllerDelegate{
//
////   func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
////
////             let cico = url as URL
////             print(cico)
////             print(url)
////
////             print(url.lastPathComponent)
////
////             print(url.pathExtension)
////      txtresume.text = url.lastPathComponent
////      ResumePath = "\(url)"
////            }
//
//  func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
//         controller.dismiss(animated: true, completion: nil)
//     }
//    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
//        return self
//    }
//}
