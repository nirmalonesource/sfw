//
//  JobPostViewController.swift
//  SFW
//
//  Created by My Mac on 02/10/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import iOSDropDown
import TagListView
import SideMenu
import IBAnimatable
import M13Checkbox
import SwiftyPickerPopover

class JobPostViewController: UIViewController,TagListViewDelegate {

    @IBOutlet var txtjobtitle: SkyFloatingLabelTextField!
    @IBOutlet var txtpersonincharge: SkyFloatingLabelTextField!
    @IBOutlet var txtmono: SkyFloatingLabelTextField!
    @IBOutlet var txtemail: SkyFloatingLabelTextField!
    @IBOutlet var txtaddress: SkyFloatingLabelTextField!
    @IBOutlet var txtskill: DropDown!
    @IBOutlet var txtworkexp: DropDown!
    @IBOutlet var txtqualification: DropDown!
    @IBOutlet var txtjobtime: SkyFloatingLabelTextField!
    @IBOutlet var txtpayscale: SkyFloatingLabelTextField!
    @IBOutlet var txtkeywords: SkyFloatingLabelTextField!
  //  @IBOutlet var txtdescrpn: SkyFloatingLabelTextField!
    @IBOutlet weak var txtdescrpn: AnimatableTextView!
    @IBOutlet var txtcity: DropDown!
       @IBOutlet var txtstate: DropDown!
       @IBOutlet var txtzipcode: DropDown!
    
    @IBOutlet var txtjobtype: DropDown!
    @IBOutlet var txtstartdate: SkyFloatingLabelTextField!
    @IBOutlet var txtenddate: SkyFloatingLabelTextField!
    @IBOutlet var txtduration: DropDown!
    @IBOutlet var txtpaymentmode: DropDown!
    @IBOutlet var txtjobopenfor: DropDown!
    @IBOutlet var chkno: M13Checkbox!
    @IBOutlet var chkyes: M13Checkbox!
    @IBOutlet var chkphone: M13Checkbox!
    @IBOutlet var chkemail: M13Checkbox!
    @IBOutlet var chkwhatsapp: M13Checkbox!
    @IBOutlet var chkwalkin: M13Checkbox!
    @IBOutlet var skilltagvw: TagListView!
    @IBOutlet weak var lbltitle: UILabel!
    
    
    var StateList = [[String:Any]]()
    var CityList = [[String:Any]]()
    var ZipcodeList = [[String:Any]]()
    var SkillList = [[String:Any]]()
    var selectedZipcodeList = [[String:Any]]()
    var selectedSkillList = [[String:Any]]()
    var StateID = String()
    var CityID = String()
    var ZipcodeID = String()
    var QualificationID = String()
    var WorkEXList = [String]()
    var strradio = ""
    var strchk = ""
    
    var selectedEditSkillList = [String]()
    var editdict = [String:Any]()
    var isedit = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SetupRadioButton()
        setdata()
        DropdownClick()
        StateApi()
        StandardListApi()
        SkillApi()
        SetupTaglistview()
        
        if isedit {
           // btnsubmit.setTitle("UPDATE", for: .normal)
//            btnprofile.isHidden = false
//            imglogo.isHidden = true
//            btnlogin.isHidden = true
//            lblaccount.isHidden = true
//            txtcontactno.isEnabled = false
            SetEditData()
        }
        else
        {
            chkno?.checkState = .checked
            strradio = "No"
        }
    }
    
    func SetEditData()  {
        
        lbltitle.text = editdict["JobTittle"] as? String
        txtjobtitle.text = editdict["JobTittle"] as? String
        txtpersonincharge.text = editdict["personincharge"] as? String
        txtmono.text = editdict["PhoneContact"] as? String
        txtemail.text = editdict["EmailIDs"] as? String
        txtaddress.text = editdict["Address1"] as? String
        txtjobtype.text = editdict["Duration"] as? String
        txtstartdate.text = editdict["StartDate"] as? String
        txtenddate.text = editdict["EndDate"] as? String
        
        let Skill = editdict["SkillIds"] as? String
        let skillarr = Skill?.components(separatedBy: ",") ?? []
        self.skilltagvw.addTags(skillarr)
        selectedEditSkillList = skillarr
        
        txtworkexp.text = editdict["Work_experience"] as? String
        txtqualification.text = editdict["Qualification"] as? String
        txtjobtime.text = editdict["JobTiming"] as? String
        
        if editdict["PaymentSchedule"] as? String == "As Per Industry Standards"
        {
           txtpayscale.isEnabled = false
        }
        else
        {
           txtpayscale.isEnabled = true
        }
        txtduration.text = editdict["PaymentSchedule"] as? String
        
        txtpayscale.text = "\(editdict["PayScale"] as? String ?? "")"
        txtpaymentmode.text = editdict["PayMode"] as? String
        txtjobopenfor.text = editdict["OpenFor"] as? String
        
        if editdict["AdditionalIncentives"] as? String == "No" {
            chkno?.checkState = .checked
            strradio = "No"
        }
        else
        {
            chkyes.checkState = .checked
            strradio = "Yes"
        }
        
        let Candidates_Contact = editdict["Candidates_Contact"] as? String ?? ""
        if Candidates_Contact.contains("Phone") {
            chkphone.checkState = .checked
        }
        if Candidates_Contact.contains("Email")  {
            chkemail.checkState = .checked
        }
        if Candidates_Contact.contains("Whatsapp")  {
            chkwhatsapp.checkState = .checked
        }
        if Candidates_Contact.contains("Walk In")   {
            chkwalkin.checkState = .checked
        }
        
        txtkeywords.text = editdict["keywords"] as? String
        txtdescrpn.text = editdict["Description"] as? String
        txtstate.text = editdict["State_Name"] as? String
        txtcity.text = editdict["City_Name"] as? String
        txtzipcode.text = editdict["ZipCode"] as? String
        
        StateID = editdict["State"] as? String ?? ""
        CityID = editdict["CityID"] as? String ?? ""
        QualificationID = editdict["Standard"] as? String ?? ""
        
        self.CityApi()
        self.ZipcodeApi()
        
          
    }
    
    func setdata()  {
        txtpersonincharge.text = "\(UserDefaults.standard.getUserDict()["PersonIncahrge"] as? String ?? "")"
         txtmono.text = "\(UserDefaults.standard.getUserDict()["mobile_no"] as? String ?? "")"
        txtemail.text = "\(UserDefaults.standard.getUserDict()["email"] as? String ?? "")"
        txtaddress.text = "\(UserDefaults.standard.getUserDict()["Address1"] as? String ?? "")"
        
        WorkEXList = ["0-2 yrs","2-4 yrs","4-6 yrs","6-8 yrs","8-10 yrs","10+ yrs"]
        txtworkexp.optionArray = WorkEXList
        txtjobtype.optionArray = ["Hourly","Daily","Weekly","Monthly"]
        txtduration.optionArray = ["Per-Hours","Daily","Weekly","Bi-Weekly","Monthly","As Per Industry Standards"]
        txtpaymentmode.optionArray = ["Cash","Cheque","Online Transfer","Demand Draft","Using UPI"]
        txtjobopenfor.optionArray = ["Male","Female","Both"]
    }
    
    func SetupTaglistview()  {
       skilltagvw.delegate = self
   }
    
    func SetupRadioButton()  {
           let chkarr = [chkno,chkyes]
             for chk in chkarr {
                 chk?.markType = .radio
                 chk?.boxType = .circle
                 chk?.tintColor = #colorLiteral(red: 0.3450980392, green: 0.6156862745, blue: 0.04705882353, alpha: 1)
                 chk?.secondaryTintColor = UIColor.lightGray
                 chk?.stateChangeAnimation = .fill
                // chk?.secondaryCheckmarkTintColor = UIColor.red
             }
       
        
        
        let chkbox = [chkphone,chkemail,chkwhatsapp,chkwalkin]
             for chk in chkbox {
                 chk?.markType = .checkmark
                 chk?.boxType = .square
                 chk?.tintColor = #colorLiteral(red: 0.3450980392, green: 0.6156862745, blue: 0.04705882353, alpha: 1)
                 chk?.secondaryTintColor = UIColor.lightGray
                 chk?.stateChangeAnimation = .fill
                // chk?.secondaryCheckmarkTintColor = UIColor.red
             }
    }
    
    func DropdownClick()  {
        
        
        txtduration.didSelect(completion: { [self] (selected, index, id)  in
                         print(selected, index, id)
                         if selected == "As Per Industry Standards"
                         {
                            txtpayscale.isEnabled = false
                         }
            else
                         {
                            txtpayscale.isEnabled = true
                         }
                     })
        
        txtqualification.didSelect(completion: { (selected, index, id)  in
                         print(selected, index, id)
                         self.QualificationID = String(id)
                     })
          
          txtzipcode.didSelect(completion: { (selected, index, id)  in
                    print(selected, index, id)
                    self.ZipcodeID = String(id)
                })
          
          txtstate.didSelect(completion: { (selected, index, id)  in
              print(selected, index, id)
              self.StateID = String(id)
              self.txtcity.text = ""
              self.CityApi()
          })
          
          txtcity.didSelect(completion: { (selected, index, id)  in
                     print(selected, index, id)
               self.CityID = String(id)
              self.txtzipcode.text = ""
            //  self.zipcodetagvw.removeAllTags()
              self.ZipcodeApi()
                 })
        
        txtskill.didSelect(completion: { (selected, index, id)  in
                          print(selected, index, id)
            if self.isedit
            {
                if !self.selectedEditSkillList.contains(selected) {
                    self.selectedEditSkillList.append(selected)
                    self.skilltagvw.addTag(selected)
                }
               
            }
            else
            {
                if self.selectedSkillList.count > 0
                           {
                           let dict:[String:Any] = self.SkillList[index]
                               
                               let index1 = self.selectedSkillList.firstIndex(where: { dictionary in
                                                          guard let value = dictionary["Skill"] as? String
                                                            else { return false }
                                                          return value == dict["Skill"] as? String
                                                        })
                                                        if let index2 = index1 {
                                                       self.selectedSkillList.remove(at: index2)
                                                         self.selectedSkillList.append(dict)
                                                        }
                     else
                                                        {
                                                          self.selectedSkillList.append(dict)
                                                           self.skilltagvw.addTag(selected)
                     }
                          
                           }
                           else
                           {
                               self.selectedSkillList.append(self.SkillList[index])
                               self.skilltagvw.addTag(selected)
                           }
            }
           
             
                      })
      }
    
    // MARK: TagListViewDelegate
    
//    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
//        print("Tag pressed: \(title), \(sender)")
//        tagView.isSelected = !tagView.isSelected
//    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
        if isedit {
           
                let index = selectedEditSkillList.firstIndex(of: title) ?? 0
                selectedEditSkillList.remove(at: index)
        }
        else
        {
                       
                let index = selectedSkillList.firstIndex(where: { dictionary in
                        guard let value = dictionary["Skill"] as? String
                        else { return false }
                        return value == title
                    })
                    if let index = index {
                        selectedSkillList.remove(at: index)
                    }
                   
        }
       
    }
    
    //MARK: - Radio Button Action
     
     @IBAction func no_click(_ sender: M13Checkbox) {
         chkno.checkState = .checked
         chkyes.checkState = .unchecked
         strradio = "No"
     }
     
     @IBAction func yes_click(_ sender: M13Checkbox) {
         chkno.checkState = .unchecked
         chkyes.checkState = .checked
         strradio = "Yes"
     }
    @IBAction func phone_click(_ sender: M13Checkbox) {
    }
    @IBAction func email_click(_ sender: M13Checkbox) {
    }
    @IBAction func whatsapp_click(_ sender: M13Checkbox) {
    }
    @IBAction func walkin_click(_ sender: M13Checkbox) {
    }
    
    
     func validation()
                {
                    if chkphone.checkState == .checked {
                        strchk.append("Phone,")
                    }
                    if chkemail.checkState == .checked {
                        strchk.append("Email,")
                    }
                    if chkwhatsapp.checkState == .checked {
                        strchk.append("Whatsapp,")
                    }
                    if chkwalkin.checkState == .checked {
                        strchk.append("Walk In,")
                    }
                    strchk = String(strchk.dropLast(1))
                    
                    self.view.endEditing(true)
                    if txtjobtitle.text == ""
                    {
                     showToast(uiview: self, msg: "Please enter Job Title")
                    }
                    else if txtjobtitle.text?.trimmingString().count ?? 0 < 3
                    {
                     showToast(uiview: self, msg: "Please enter at least 3 character for Job Title")
                    }
                    else if txtpersonincharge.text == ""
                    {
                        showToast(uiview: self, msg: "Please enter In-charge Person Name")
                    }
                    else if  txtmono.text?.count ?? 0 < 10
                    {
                        showToast(uiview: self, msg: "Please enter valid Mobile Number")
                    }
                     else if !txtemail.isValidEmail()
                    {
                        showToast(uiview: self, msg: "Please enter EmailId")
                    }
                    else if txtaddress.text == ""
                    {
                        showToast(uiview: self, msg: "Please enter Address")
                    }
//                    else if txtjobtype.text == ""
//                    {
//                        showToast(uiview: self, msg: "Please select Job Type")
//                    }
                    else if txtstartdate.text == ""
                    {
                    showToast(uiview: self, msg: "Please select Start Date")
                    }
                    // end date must be greater than start date
                    
//                    else if txtenddate.text == ""
//                    {
//                    showToast(uiview: self, msg: "Please select End Date")
//                    }
                    else if selectedSkillList.count == 0 && isedit == false
                    {
                        showToast(uiview: self, msg: "Please select Skill")
                    }
                    else if selectedEditSkillList.count == 0 && isedit == true
                    {
                        showToast(uiview: self, msg: "Please select Skill")
                    }
                    else if txtworkexp.text == ""
                    {
                        showToast(uiview: self, msg: "Please select Work Experience")
                    }
                    else if txtqualification.text == ""
                    {
                        showToast(uiview: self, msg: "Please select Qualification")
                    }
                    else if txtjobtime.text == ""
                    {
                        showToast(uiview: self, msg: "Please enter Job Time")
                    }
//                    else if txtduration.text == ""
//                    {
//                        showToast(uiview: self, msg: "Please select Job Duration")
//                    }
//                    else if txtpayscale.text == ""
//                    {
//                        showToast(uiview: self, msg: "Please Pay Scale")
//                    }
//                    else if txtpaymentmode.text == ""
//                    {
//                        showToast(uiview: self, msg: "Please Select Payment Mode")
//                    }
                    else if txtjobopenfor.text == ""
                    {
                        showToast(uiview: self, msg: "Please Select Job Open For")
                    }
                    else if txtdescrpn.text == ""
                    {
                        showToast(uiview: self, msg: "Please enter Description")
                    }
                    else if txtstate.text == ""
                    {
                        showToast(uiview: self, msg: "Please select State")
                    }
                    else if txtcity.text == ""
                    {
                        showToast(uiview: self, msg: "Please select City")
                    }
                    else if txtzipcode.text == ""
                    {
                        showToast(uiview: self, msg: "Please select Zipcode")
                    }
                    else
                    {
                        if isedit {
                            EditJobPostApi()
                        }
                        else
                        {
                            JobPostApi()
                        }
                         
                    }
                }
    
    
      // MARK: - API
    
    func SkillApi()
                                    {
                                        if !isInternetAvailable(){
                                            noInternetConnectionAlert(uiview: self)
                                        }
                                        else
                                        {
                                           
                                       let parameters = ["CityID":CityID] as [String : Any]
                                         
                                         let url = ServiceList.SERVICE_URL+ServiceList.STATE_SKILL_LIST_API
                                         
                                             callApi(url,
                                                     method: .get,
                                                     param: parameters ,
                                                     withLoader: true)
                                                   { (result) in
                                                        print("\(ServiceList.STATE_SKILL_LIST_API)==>RESPONSE:",result)
                                                        if result.getBool(key: "status")
                                                        {
                                                            let dict = result["data"] as? [String:Any] ?? [:]
                                                            self.SkillList = dict.getArrayofDictionary(key: "Skill")
                                                            
                                                            self.txtskill.optionArray = self.SkillList.compactMap {"\($0["Skill"] as? String ?? "0")"}
                                                            self.txtskill.optionIds =  self.SkillList.compactMap {Int( $0["SkillID"] as? String ?? "0")}
                                                        }
                                                     else
                                                        {
                                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                                     }
                                            }
                                        }
                                        
                                    }
    
      func StandardListApi()
                                    {
                                        if !isInternetAvailable(){
                                            noInternetConnectionAlert(uiview: self)
                                        }
                                        else
                                        {
                                           
                                        let parameters = ["ROLEID":"3"] as [String : Any]
                                         
                                         let url = ServiceList.SERVICE_URL+ServiceList.STATE_STANDARD_LIST_API
                                         
                                             callApi(url,
                                                     method: .post,
                                                     param: parameters ,
                                                     withLoader: true)
                                                   { (result) in
                                                        print("\(ServiceList.STATE_STANDARD_LIST_API)==>RESPONSE:",result)
                                                        if result.getBool(key: "status")
                                                        {
                                                            let dict = result["data"] as? [String:Any] ?? [:]
                                                            let arr = dict.getArrayofDictionary(key: "Standard")
                                                            
                                                            self.txtqualification.optionArray = arr.compactMap {"\($0["Standard"] as? String ?? "0")"}
                                                            self.txtqualification.optionIds =  arr.compactMap {Int( $0["StandardID"] as? String ?? "0")}
                                                        }
                                                     else
                                                        {
                                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                                     }
                                            }
                                        }
                                        
                                    }
    
      func StateApi()
                            {
                                if !isInternetAvailable(){
                                    noInternetConnectionAlert(uiview: self)
                                }
                                else
                                {
                                   
                               let parameters = [:] as [String : Any]
                                 
                                 let url = ServiceList.SERVICE_URL+ServiceList.STATELIST_API
                                 
                                     callApi(url,
                                             method: .get,
                                             param: parameters ,
                                             withLoader: true)
                                           { (result) in
                                                print("\(ServiceList.STATELIST_API)==>RESPONSE:",result)
                                                if result.getBool(key: "status")
                                                {
                                                    let dict = result["data"] as? [String:Any] ?? [:]
                                                    self.StateList = dict.getArrayofDictionary(key: "state")
                                                    self.txtstate.optionArray = self.StateList.compactMap {"\($0["Name"] as? String ?? "0")"}
                                                    self.txtstate.optionIds =  self.StateList.compactMap {Int( $0["ID"] as? String ?? "0")}
                                                }
                                             else
                                                {
                                                 showToast(uiview: self, msg: result.getString(key: "message"))
                                             }
                                    }
                                }
                                
                            }
        
        func CityApi()
                               {
                                   if !isInternetAvailable(){
                                       noInternetConnectionAlert(uiview: self)
                                   }
                                   else
                                   {
                                      
                                  let parameters = ["StateID":StateID] as [String : Any]
                                    
                                    let url = ServiceList.SERVICE_URL+ServiceList.STATE_CITY_API
                                    
                                        callApi(url,
                                                method: .post,
                                                param: parameters ,
                                                withLoader: true)
                                              { (result) in
                                                   print("\(ServiceList.STATE_CITY_API)==>RESPONSE:",result)
                                                   if result.getBool(key: "status")
                                                   {
                                                       let dict = result["data"] as? [String:Any] ?? [:]
                                                       self.CityList = dict.getArrayofDictionary(key: "city")
                                                       self.txtcity.optionArray = self.CityList.compactMap {"\($0["Name"] as? String ?? "0")"}
                                                       self.txtcity.optionIds =  self.CityList.compactMap {Int( $0["ID"] as? String ?? "0")}
                                                   }
                                                else
                                                   {
                                                    showToast(uiview: self, msg: result.getString(key: "message"))
                                                }
                                       }
                                   }
                                   
                               }
        
        func ZipcodeApi()
                                 {
                                     if !isInternetAvailable(){
                                         noInternetConnectionAlert(uiview: self)
                                     }
                                     else
                                     {
                                        
                                    let parameters = ["CityID":CityID] as [String : Any]
                                      
                                      let url = ServiceList.SERVICE_URL+ServiceList.STATE_ZIPCODE_API
                                      
                                          callApi(url,
                                                  method: .post,
                                                  param: parameters ,
                                                  withLoader: true)
                                                { (result) in
                                                     print("\(ServiceList.STATE_ZIPCODE_API)==>RESPONSE:",result)
                                                     if result.getBool(key: "status")
                                                     {
                                                         let dict = result["data"] as? [String:Any] ?? [:]
                                                         self.ZipcodeList = dict.getArrayofDictionary(key: "Zipcode")
                                                        
                                                         self.txtzipcode.optionArray = self.ZipcodeList.compactMap {"\($0["ZipCode"] as? String ?? "0")"}
                                                         self.txtzipcode.optionIds =  self.ZipcodeList.compactMap {Int( $0["ZipcodeID"] as? String ?? "0")}
                                                     }
                                                  else
                                                     {
                                                      showToast(uiview: self, msg: result.getString(key: "message"))
                                                  }
                                         }
                                     }
                                     
                                 }
    
    func JobPostApi()
                    {
                        if !isInternetAvailable(){
                            noInternetConnectionAlert(uiview: self)
                        }
                        else
                        {
                            let skillidarr = self.selectedSkillList.compactMap {"\($0["Skill"] as? String ?? "0")"}
                            
                            let strskill = skillidarr.joined(separator: ",")
                            
                     //    var parameters = [:] as [String : Any]
                            
                            let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                          "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                                               ] as [String : Any]
                            
                            let  chk1 = [
                                "SkillIds" : strskill,
                                "JobTiming" : txtjobtime.text ?? "",
                                "JobTittle" : txtjobtitle.text ?? "",
                                "personincharge" : txtpersonincharge.text ?? "",
                                "PhoneContact" :  txtmono.text!,
                                "Description" :  txtdescrpn.text!,
                                "Work_experience" : txtworkexp.text!,
                                "Qualification" : QualificationID,
                                "EmailIDs" :txtemail.text!,
                                "Address1" : txtaddress.text!
                         ] as [String : Any]
                            
                            let chk2 = [
                                   "CityID" :  CityID,
                                   "State" : StateID,
                                   "ZipCode" :  txtzipcode.text!,
                                   "PayMode" : txtpaymentmode.text ?? "",
                                   "keywords" : txtkeywords.text ?? "",
                                   "PayScale" : txtpayscale.text ?? "",
                                   "StartDate" : txtstartdate.text ?? "",
                                   "EndDate" : txtenddate.text ?? "",
                                   "PaymentSchedule" : txtduration.text ?? "",
                                   "Candidates_Contact" : strchk,
                                   "Duration" : txtjobtype.text ?? "",
                                   "AdditionalIncentives" : strradio,
                                    "OpenFor" : txtjobopenfor.text ?? ""
                            ] as [String : Any]
                            
                            let parameters = chk1.merging(chk2) { (current, _) in current }
                            
                         
                         let url = ServiceList.SERVICE_URL+ServiceList.JOBPOST_API
                         
                             callApi(url,
                                     method: .post,
                                     param: parameters ,
                                     extraHeader: header,
                                     withLoader: true)
                                   { (result) in
                                     print("\(ServiceList.JOBPOST_API)==>RESPONSE:",result)
                                        if result.getBool(key: "status")
                                        {
                                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyDashboardViewController") as! CompanyDashboardViewController
                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                            showToast(uiview: self, msg: result.getString(key: "message"))
                                        }
                                     else
                                        {
                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                     }
                            }
                        }
                        
                    }
    
    func EditJobPostApi()
                    {
                        if !isInternetAvailable(){
                            noInternetConnectionAlert(uiview: self)
                        }
                        else
                        {
                            let strskill = selectedEditSkillList.joined(separator: ",")
                            
                     //    var parameters = [:] as [String : Any]
                            
                            let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                          "X-STUDENT-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                                               ] as [String : Any]
                            
                            let  chk1 = [
                                "JobID" : editdict["JobID"] ?? "",
                                "SkillIds" : strskill,
                                "JobTiming" : txtjobtime.text ?? "",
                                "JobTittle" : txtjobtitle.text ?? "",
                                "personincharge" : txtpersonincharge.text ?? "",
                                "PhoneContact" :  txtmono.text!,
                                "Description" :  txtdescrpn.text!,
                                "Work_experience" : txtworkexp.text!,
                                "Qualification" : QualificationID,
                                "EmailIDs" :txtemail.text!,
                                "Address1" : txtaddress.text!
                         ] as [String : Any]
                            
                            let chk2 = [
                                   "CityID" :  CityID,
                                   "State" : StateID,
                                   "ZipCode" :  txtzipcode.text!,
                                   "PayMode" : txtpaymentmode.text ?? "",
                                   "keywords" : txtkeywords.text ?? "",
                                   "PayScale" : txtpayscale.text ?? "",
                                   "StartDate" : txtstartdate.text ?? "",
                                   "EndDate" : txtenddate.text ?? "",
                                   "PaymentSchedule" : txtduration.text ?? "",
                                   "Candidates_Contact" : strchk,
                                   "Duration" : txtjobtype.text ?? "",
                                   "AdditionalIncentives" : strradio,
                                    "OpenFor" : txtjobopenfor.text ?? ""
                            ] as [String : Any]
                            
                            let parameters = chk1.merging(chk2) { (current, _) in current }
                            
                         
                         let url = ServiceList.SERVICE_URL+ServiceList.JOB_EDIT_API
                         
                             callApi(url,
                                     method: .post,
                                     param: parameters ,
                                     extraHeader: header,
                                     withLoader: true)
                                   { (result) in
                                     print("\(ServiceList.JOBPOST_API)==>RESPONSE:",result)
                                        if result.getBool(key: "status")
                                        {
                                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyDashboardViewController") as! CompanyDashboardViewController
                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                            showToast(uiview: self, msg: result.getString(key: "message"))
                                        }
                                     else
                                        {
                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                     }
                            }
                        }
                        
                    }
    

      //MARK: - Button Action
    
      @IBAction func menu_click(_ sender: UIButton) {
//              let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! SideMenuNavigationController
//                    menu.presentationStyle.menuStartAlpha = 1.0
//                    menu.presentationStyle.presentingEndAlpha = 0.5
//                    present(menu, animated: true, completion: nil)
        
          self.navigationController?.popViewController(animated: true)
         }
      
    @IBAction func startdate_click(_ sender: UIButton) {
        
        self.view.endEditing(true)
                 DatePickerPopover(title: "Select DOB")
                .setMinimumDate(Date())
                .setDateMode(.date)
                .setSelectedDate(Date())
                .setDoneButton(action: {
                     popover, selectedDate in
                     
                     print("selectedDate \(selectedDate)")
                     let formatter = DateFormatter()
                         formatter.dateFormat = "dd-MM-yyyy"
                     self.txtstartdate.text = formatter.string(from: selectedDate)
                    self.txtenddate.text = ""
//                     let age = calcAge(birthday: formatter.string(from: selectedDate))
//                     if age > 18
//                     {
//                         self.txtdob.text = formatter.string(from: selectedDate)
//                     }
//                     else
//                     {
//                         DispatchQueue.main.async {
//                             showToast(uiview: self, msg: "Age must be 18 year old")
//                         }
//                     }
                 })
                 .setCancelButton(action: { _, _ in print("cancel")})
                 .appear(originView: sender, baseViewController: self)
    }
    
    @IBAction func enddate_click(_ sender: UIButton) {
        let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
        var maxdate = String()
        if txtstartdate.text != "" {
            maxdate = txtstartdate.text ?? ""
        }
        else
        {
            maxdate = formatter.string(from: Date())
        }
     
         self.view.endEditing(true)
            DatePickerPopover(title: "Select DOB")
            .setMinimumDate(formatter.date(from: maxdate)!)
            .setDateMode(.date)
        //  .setSelectedDate(Date())
            .setDoneButton(action: {
                             popover, selectedDate in
                             
                             print("selectedDate \(selectedDate)")
                            
                             self.txtenddate.text = formatter.string(from: selectedDate)
        //                     let age = calcAge(birthday: formatter.string(from: selectedDate))
        //                     if age > 18
        //                     {
        //                         self.txtdob.text = formatter.string(from: selectedDate)
        //                     }
        //                     else
        //                     {
        //                         DispatchQueue.main.async {
        //                             showToast(uiview: self, msg: "Age must be 18 year old")
        //                         }
        //                     }
                         })
                         .setCancelButton(action: { _, _ in print("cancel")})
                         .appear(originView: sender, baseViewController: self)
    }
    
      @IBAction func post_click(_ sender: AnimatableButton) {
            validation()
      }

}
